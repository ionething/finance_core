/*
 *  @Title CacheManager.java
 *  @Package： com.phoenix.cache.objcache
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.cache.objcache;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.phoenix.cache.general.CacheObject;
import com.phoenix.cache.general.LinkedList;
import com.phoenix.cache.general.LinkedListNode;

/**
 *  @ClassName CacheManager
 *  @Description 系统本地缓存实现调用直接初始化如缓存设定都为通用即通用也可以用同样内部实现稍微有点点区别增加了运行时修改缓存大小与可以定义对象保留时间
 *  @author liuwb
 *  @version 1.0
 *  @date 2017年7月26日
 */
public class CacheManager<E> {

	protected Map<Object, CacheObject<E>> cachedObjectsHash;

	protected LinkedList lastAccessedList;

	protected int size = 0;

	protected final Lock lock = new ReentrantLock();

	/**
	 * 整个缓存的大小(粗粒度只控制缓存个数)
	 */
	protected int maxSize = 1024;

	/**
	 * Create a new cache with default values. 用默认大小创建一个缓存池
	 */
	public CacheManager() {
		cachedObjectsHash = new ConcurrentHashMap<Object, CacheObject<E>>();
		lastAccessedList = new LinkedList();
	}

	/**
	 * Create a new cache and specify the maximum size for the cache 指定最大值创建缓存池
	 * 
	 * @param maxSize
	 *            the maximum size of the cache
	 */
	public CacheManager(int maxSize) {
		this();
		this.maxSize = maxSize;
	}

	/**
	 * Returns the current size of the cache
	 * 
	 * @return the size of the cache
	 */
	public int getSize() {
		return size > cachedObjectsHash.size() ? size : cachedObjectsHash.size();
	}

	/**
	 * 最大限制
	 */
	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
		cullCache();
	}

	/**
	 * 向池中添加一个关键字为Key的缓存对象object
	 * 
	 * @param key
	 *            a unique key for the object being put into cache.
	 * @param object
	 *            the CacheObj object to put into cache.
	 */
	public void add(Object key, CacheObj<E> object) {
		// 先把原来的对象remove掉
		remove(key);
		try {
			lock.lock();
			// 新建一个缓存对象，并放入哈希表中
			CacheObject<E> cacheObject = new CacheObject<E>(object);
			cachedObjectsHash.put(key, cacheObject);
			size++;
			// 把缓存元素的Key放到lastAccessed List链表的最前面
			LinkedListNode lastAccessedNode = lastAccessedList.addFirst(key);
			// Store the cache order list entry so that we can get back to it
			cacheObject.lastAccessedListNode = lastAccessedNode;
		} finally {
			lock.unlock();
		}
		cullCache();
	}

	/**
	 * 从池中得到一个关键字为Key的缓存对象object conditions:
	 * <ul>
	 * <li>The object referenced by the key was never added to cache.
	 * <li>The object referenced by the key has expired from cache.
	 * </ul>
	 * 
	 * @param key
	 *            the unique key of the object to get.
	 * @return the CacheObj object corresponding to unique key.
	 */
	public CacheObj<E> get(Object key) {
		// 清理过期对象
		deleteExpiredEntries();
		CacheObject<E> cacheObject = null;
		if (cachedObjectsHash.containsKey(key)) {
			try {
				lock.lock();
				cacheObject = cachedObjectsHash.get(key);
				// 将该缓存对象从lastAccessedList链表中取下并插入到
				// 链表头部
				cacheObject.lastAccessedListNode.remove();
				lastAccessedList.addFirst(cacheObject.lastAccessedListNode);
			} finally {
				lock.unlock();
			}
			return cacheObject.object;
		}
		return null;
	}

	/**
	 * Removes an object from cache.
	 * 
	 * @param key
	 *            the unique key of the object to remove.
	 */
	public void remove(Object key) {
		// If the object is not in cache, stop trying to remove it.
		if (!cachedObjectsHash.containsKey(key))
			return;
		try {
			lock.lock();
			CacheObject<E> cacheObject = cachedObjectsHash.get(key);
			if (cacheObject == null) {
				return;
			}
			// 移除缓存
			cachedObjectsHash.remove(key);
			size--;
			// 移除缓存对象访问链表
			cacheObject.lastAccessedListNode.remove();
			cacheObject.lastAccessedListNode = null;
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Clears the cache of all objects. The size of the cache is reset to 0.
	 */
	public void clear() {
		try {
			lock.lock();
			Object[] keys = cachedObjectsHash.keySet().toArray();
			for (int i = 0; i < keys.length; i++) {
				remove(keys[i]);
			}
			cachedObjectsHash.clear();
			cachedObjectsHash = new ConcurrentHashMap<Object, CacheObject<E>>();
			lastAccessedList.clear();
			lastAccessedList = new LinkedList();
			size = 0;
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Returns an array of the keys contained in the cache.
	 * 
	 * @return an array of the keys present in the cache.
	 */
	public Object[] keys() {
		return cachedObjectsHash.keySet().toArray();
	}

	/**
	 * Returns an array of the values contained in the cache.
	 * 
	 * @return a array of the cache entries.
	 */
	public List<CacheObject<E>> values() {
		List<CacheObject<E>> values = new ArrayList<CacheObject<E>>();
		Iterator<Entry<Object, CacheObject<E>>> it = cachedObjectsHash.entrySet().iterator();
		while (it.hasNext()) {
			values.add(it.next().getValue());
		}
		return values;
	}

	/**
	 * 移除过期缓存
	 */
	private final void deleteExpiredEntries() {

	}

	/**
	 * 移除缓存,先进后出,即移除最早访问的
	 */
	synchronized private final void cullCache() {
		if (size >= maxSize) {
			deleteExpiredEntries();
			int desiredSize = maxSize;
			while (size >= desiredSize) {
				remove(lastAccessedList.getLast().getValue());
			}
		}
	}

	public Map<Object, CacheObject<E>> getValue() {
		return cachedObjectsHash;
	}
}
