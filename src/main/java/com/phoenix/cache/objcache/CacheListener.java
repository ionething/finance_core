/*
 *  @Title CacheListener.java
 *  @Package： com.phoenix.cache.objcache
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.cache.objcache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *  @ClassName CacheListener
 *  @Description 缓存对象管理
 *  @author yijun
 *  @version 1.0
 *  @date 2017年7月16日
 */
public class CacheListener<T> {

	/***************************************************************************
	 * 对象的封装类
	 **************************************************************************/
	class CacheNode {
		CacheNode prev;
		CacheNode next;
		CacheObj<T> value;
		String key;

		public CacheNode() {

		}
	}

	/***************************************************************************
	 * 对象池大小
	 **************************************************************************/
	private int cacheSize;
	/***************************************************************************
	 * 对象列表
	 **************************************************************************/
	private Map<String, CacheNode> nodes;

	/***************************************************************************
	 * 对象池当前对象数
	 **************************************************************************/
	private volatile int currentSize = 0;
	/***************************************************************************
	 * 第一个节点
	 **************************************************************************/
	private CacheNode first;
	/***************************************************************************
	 * 最后一个节点
	 **************************************************************************/
	private CacheNode last;
	/***************************************************************************
	 * 对象锁
	 **************************************************************************/
	private static int DEFAULT_SIZE = 16;

	// private byte[] lock = new byte[0];

	protected final Lock lock = new ReentrantLock();

	public CacheListener() {
		this(DEFAULT_SIZE);
	}

	public CacheListener(int poolSize) {
		cacheSize = poolSize;
		currentSize = 0;
		first = null;
		last = null;
		nodes = new ConcurrentHashMap<String, CacheNode>(poolSize);
	}

	/***************************************************************************
	 * 读取一个对象
	 **************************************************************************/
	public CacheObj<T> get(String key) {
		try {
			lock.lock();
			CacheNode node = nodes.get(key);
			if (node != null) {
				moveToHead(node);
				return node.value;
			} else {
				return null;
			}
		} finally {
			lock.unlock();
		}
	}

	/***************************************************************************
	 * 是否包含对象
	 **************************************************************************/
	public boolean containsKey(String key) {
		if (key == null)
			return false;
		return nodes.containsKey(key);
	}

	public boolean isEmpty() {
		return currentSize == 0 || nodes.isEmpty();
	}

	/***************************************************************************
	 * 把指定对象移动到链表的头部
	 **************************************************************************/
	private void moveToHead(CacheNode node) {
		if (node == first) {
			return;
		}
		if (node.prev != null) {
			node.prev.next = node.next;
		}
		if (node.next != null) {
			node.next.prev = node.prev;
		}
		if (last == node) {
			last = node.prev;
		}
		if (first != null) {
			node.next = first;
			first.prev = node;
		}
		first = node;
		node.prev = null;
		if (last == null) {
			last = first;
		}
	}

	/***************************************************************************
	 * 删除池中指定对象
	 **************************************************************************/
	public CacheNode remove(String key) {
		try {
			lock.lock();
			CacheNode node = nodes.get(key);
			if (node != null) {
				if (node.prev != null) {
					node.prev.next = node.next;
				}
				if (node.next != null) {
					node.next.prev = node.prev;
				}
				if (last == node) {
					last = node.prev;
				}
				if (first == node) {
					first = node.next;
				}
				nodes.remove(key);
				currentSize--;
			}
			return node;
		} finally {
			lock.unlock();
		}
	}

	/***************************************************************************
	 * 放置一个对象到池中
	 **************************************************************************/
	public void put(String key, CacheObj<T> value) {
		try {
			lock.lock();
			CacheNode node = nodes.get(key);
			if (node == null) {
				if (currentSize >= cacheSize) {
					if (last != null) {
						nodes.remove(last.key);
					}
					removeLast();
				} else {
					currentSize++;
				}
				node = getNewCacheNode();
			}
			node.value = value;
			node.key = key;
			moveToHead(node);
			nodes.put(key, node);
		} finally {
			lock.unlock();
		}
	}

	/***************************************************************************
	 * 清空池中对象
	 **************************************************************************/
	public void clear() {
		try {
			lock.lock();
			while (!nodes.isEmpty()) {
				if (last != null) {
					nodes.remove(last.key);
					currentSize--;
				}
				removeLast();
			}
			first = null;
			last = null;
		} finally {
			lock.unlock();
		}
	}

	/***************************************************************************
	 * 获得一个新对象
	 **************************************************************************/
	private CacheNode getNewCacheNode() {
		CacheNode node = new CacheNode();
		return node;
	}

	/***************************************************************************
	 * 删除池中最久没有使用的对象
	 **************************************************************************/
	private void removeLast() {
		if (last != null) {
			try {
				lock.lock();
				last.value.doClear();
				if (last.prev != null) {
					last.prev.next = null;
				} else {
					first = null;
				}
				last = last.prev;
			} finally {
				lock.unlock();
			}
		}
	}
}
