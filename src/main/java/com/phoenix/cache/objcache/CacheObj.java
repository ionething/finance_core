/*
 *  @Title CacheObj.java
 *  @Package： com.phoenix.cache.objcache
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.cache.objcache;

/**
 *  @ClassName CacheObj
 *  @Description 系统缓存对象接口所有实现接口的实例都可以作为系统缓存存入缓存对象池管理
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年6月12日
 */
public interface CacheObj<T> {

	/**
	 * 清除对象
	 */
	public void doClear();

	/**
	 * 返回值
	 * 
	 * @return T
	 */
	public T getValue();

}
