/*
 *  @Title LinkedListNode.java
 *  @Package： com.phoenix.cache.general
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.cache.general;

/**
 *  @ClassName LinkedListNode
 *  @Description 链表节点对象
 *  @author liuwenbin
 *  @version 1.0
 *  @date 2017年7月9日
 */
public class LinkedListNode {

	public LinkedListNode previous;

	public LinkedListNode next;

	Object object;

	public long timestamp;

	/**
	 * Constructs a new linked list node.
	 * 
	 * @param object
	 *            the Object that the node represents.
	 * @param next
	 *            a reference to the next LinkedListNode in the list.
	 * @param previous
	 *            a reference to the previous LinkedListNode in the list.
	 */
	public LinkedListNode(Object object, LinkedListNode next, LinkedListNode previous) {
		this.object = object;
		this.next = next;
		this.previous = previous;
	}

	public Object getValue() {
		return object;
	}

	/**
	 * Removes this node from the linked list that it is a part of.
	 */
	public void remove() {
		previous.next = next;
		next.previous = previous;
	}

	/**
	 * Returns a String representation of the linked list node by calling the
	 * toString method of the node's object.
	 * 
	 * @return a String representation of the LinkedListNode.
	 */
	public String toString() {
		return object.toString();
	}
}
