/*
 *  @Title CacheObject.java
 *  @Package： com.phoenix.cache.general
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.cache.general;

import com.phoenix.cache.objcache.CacheObj;

/**
 *  @ClassName CacheObject
 *  @Description 缓存对象
 *  @author liuwei
 *  @version 1.0
 *  @date 2017年4月25日
 */
public final class CacheObject<T> {

	/**
	 * 缓存对象
	 */
	public CacheObj<T> object;

	/**
	 * 记录访问的链表
	 */
	public LinkedListNode lastAccessedListNode;

	/**
	 * 缓存命中次数
	 */
	protected long cacheHits, cacheMisses = 0L;

	/**
	 * 暂存用来作存放时间 暂无实现
	 */
	public LinkedListNode ageListNode;

	/**
	 * 存放的缓存对象
	 * <p>
	 * 
	 * @param object
	 *            缓存对象
	 */
	public CacheObject(CacheObj<T> object) {
		this.object = object;
	}

	public void Hit() {
		cacheHits++;
	}

	public void Miss() {
		cacheMisses++;
	}

	public boolean isValueable() {
		return cacheHits > cacheMisses;
	}

	public void Reset() {
		cacheHits = 0L;
		cacheMisses = 0L;
	}
}
