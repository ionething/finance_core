/*
 *  @Title HttpClientHelper.java
 *  @Package： com.phoenix.core.httpclient
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.httpclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.SerializableEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.phoenix.core.httpclient.exception.HttpClientException;
import com.phoenix.core.logger.SimpleLogger;

/**
 *  @ClassName HttpClientHelper
 *  @Description 请求类
 *  @author liuwenbin
 *  @version 1.0
 *  @date 2017年4月20日
 */
public class HttpClientHelper {

	public final int MAX_FETCHSIZE = 500000;

	private final SimpleLogger logger = SimpleLogger.getLogger(HttpClientHelper.class);

	static class SingletonHolder {
		static HttpClientHelper instance = new HttpClientHelper();
	}

	public static HttpClientHelper getInstance() {
		return SingletonHolder.instance;
	}

	private HttpClientHelper() {
	}

	/**
	 * 发送httpPost请求，请内容为map集合（需指定key-value）
	 * 
	 * @param url
	 *            请求地址
	 * @param charset
	 *            请求/响应字符编码
	 * @param map
	 *            参数列表
	 * @return 响应结果
	 */
	public String sendHttpPost(String url, Charset charset, Map<String, String> map) throws HttpClientException {
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		try {
			if (logger.isDebugEnabled())
				logger.debug(" send http start ");
			if (StringUtils.isBlank(url))
				throw new HttpClientException(" sendHttpPost exception,url is blank.");
			if (charset == null)
				charset = Charset.forName("UTF-8");
			httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url.trim());
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			for (Entry<String, String> entry : map.entrySet())
				nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nvps, charset);
			httpPost.setConfig(this.getCustomConfig());
			httpPost.setEntity(urlEncodedFormEntity);
			// 执行请求，得到响应对象
			response = httpclient.execute(httpPost);
			if (logger.isDebugEnabled())
				logger.debug(" send http execute status is: " + response.getStatusLine());
			// 得到响应实体
			HttpEntity entity = response.getEntity();
			// 转换成字符串
			String resultXml = EntityUtils.toString(entity, charset);
			// 检查是否读取完毕
			EntityUtils.consume(entity);
			if (logger.isDebugEnabled())
				logger.debug(" send http end ");
			return resultXml != null && !"".equals(resultXml) ? resultXml : "";
		} catch (Exception e) {
			logger.error("sendHttpPost exception:", e);
			throw new HttpClientException(" sendHttpPost exception: ", e);
		} finally {
			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					logger.error("sendHttpPost response close IOException:", e);
				}
			}
			if (httpclient != null) {
				try {
					httpclient.close();
				} catch (IOException e) {
					logger.error("sendHttpPost httpclient close IOException:", e);
				}
			}
		}
	}

	/**
	 * 发送httpPost,application/json格式请求，请内容为map集合（需指定key-value）
	 * 
	 * @param url
	 *            请求地址
	 * @param charset
	 *            请求/响应字符编码
	 * @param jsonStr
	 *            json字符串
	 * @return 响应结果
	 */
	public String sendHttpPostByJson(String url, Charset charset, String jsonStr) throws HttpClientException {
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		try {
			if (logger.isDebugEnabled())
				logger.debug(" send http start ");
			if (StringUtils.isBlank(url))
				throw new HttpClientException(" sendHttpPost exception,url is blank.");
			if (charset == null)
				charset = Charset.forName("UTF-8");
			httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url.trim());

			httpPost.setConfig(this.getCustomConfig());
			StringEntity se = new StringEntity(jsonStr);
			httpPost.setEntity(se);
			httpPost.setHeader("Content-Type", "application/json");
			// 执行请求，得到响应对象
			response = httpclient.execute(httpPost);
			if (logger.isDebugEnabled())
				logger.debug(" send http execute status is: " + response.getStatusLine());
			// 得到响应实体
			HttpEntity entity = response.getEntity();
			// 转换成字符串
			String resultXml = EntityUtils.toString(entity, charset);
			// 检查是否读取完毕
			EntityUtils.consume(entity);
			if (logger.isDebugEnabled())
				logger.debug(" send http end ");
			return resultXml != null && !"".equals(resultXml) ? resultXml : "";
		} catch (Exception e) {
			logger.error("sendHttpPost exception:", e);
			throw new HttpClientException(" sendHttpPost exception: ", e);
		} finally {
			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					logger.error("sendHttpPost response close IOException:", e);
				}
			}
			if (httpclient != null) {
				try {
					httpclient.close();
				} catch (IOException e) {
					logger.error("sendHttpPost httpclient close IOException:", e);
				}
			}
		}
	}

	/**
	 * 发送httpPost请求，请求内容为字符串
	 * 
	 * @param url
	 *            请求地址
	 * @param charset
	 *            请求/响应字符编码
	 * @param str
	 *            传递内容
	 * @return 响应结果
	 */
	public String sendHttpPost(String url, Charset charset, String str) throws HttpClientException {
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		try {
			if (logger.isDebugEnabled())
				logger.debug(" send http start ");
			if (StringUtils.isBlank(url))
				throw new HttpClientException(" sendHttpPost exception,url is blank.");
			if (charset == null)
				charset = Charset.forName("UTF-8");
			httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url.trim());
			StringEntity stringEntity = new StringEntity(str, charset);
			httpPost.setConfig(this.getCustomConfig());
			httpPost.setEntity(stringEntity);
			response = httpclient.execute(httpPost);
			if (logger.isDebugEnabled())
				logger.debug(" send http execute status is: " + response.getStatusLine());
			HttpEntity entity = response.getEntity();
			String resultXml = EntityUtils.toString(entity, charset);
			EntityUtils.consume(entity);
			if (logger.isDebugEnabled())
				logger.debug(" send request end ");
			return resultXml != null && !"".equals(resultXml) ? resultXml : "";
		} catch (Exception e) {
			logger.error("sendHttpPost exception:", e);
			throw new HttpClientException(" sendHttpPost exception:", e);
		} finally {
			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					logger.error("sendHttpPost response close IOException:", e);
				}
			}
			if (httpclient != null) {
				try {
					httpclient.close();
				} catch (IOException e) {
					logger.error("sendHttpPost httpclient close IOException:", e);
				}
			}
		}
	}

	/**
	 * 发送http get请求
	 * 
	 * @param url
	 *            请求地址
	 * @param charset
	 *            请求/响应字符编码
	 * @return 响应结果
	 */
	public String sendHttpGet(String url, Charset charset) throws HttpClientException {
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		try {
			if (logger.isDebugEnabled())
				logger.debug(" send http start ");
			if (StringUtils.isBlank(url))
				throw new HttpClientException(" sendHttpGet exception,url is blank.");
			if (charset == null)
				charset = Charset.forName("UTF-8");
			httpclient = HttpClients.createDefault();
			HttpGet httpget = new HttpGet(url.trim());
			httpget.setConfig(this.getCustomConfig());
			response = httpclient.execute(httpget);
			if (logger.isDebugEnabled())
				logger.debug(" send http execute status is: " + response.getStatusLine());
			HttpEntity entity = response.getEntity();
			String resultXml = EntityUtils.toString(entity, charset);
			EntityUtils.consume(entity);
			if (logger.isDebugEnabled())
				logger.debug(" send request end ");
			return resultXml != null && !"".equals(resultXml) ? resultXml : "";
		} catch (Exception e) {
			logger.error("sendHttpGet exception:", e);
			throw new HttpClientException(" sendHttpGet exception:", e);
		} finally {
			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					logger.error("sendHttpGet response close IOException:", e);
				}
			}
			if (httpclient != null) {
				try {
					httpclient.close();
				} catch (IOException e) {
					logger.error("sendHttpGet httpclient close IOException:", e);
				}
			}
		}
	}

	/**
	 * 发送httpPost请求，请求内容为流对象
	 * 
	 * @param url
	 *            请求地址
	 * @param charset
	 *            响应字符编码
	 * @param inputStream
	 *            流对象
	 * @return 响应结果
	 */
	public String sendHttpPost(String url, Charset charset, InputStream inputStream) throws HttpClientException {
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		try {
			if (logger.isDebugEnabled())
				logger.debug(" send http start ");
			if (StringUtils.isBlank(url))
				throw new HttpClientException(" sendHttpPost exception,url is blank.");
			if (charset == null)
				charset = Charset.forName("UTF-8");
			httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url.trim());
			InputStreamEntity inputStreamEntity = new InputStreamEntity(inputStream, -1);
			inputStreamEntity.setContentType("binary/octet-stream");
			inputStreamEntity.setChunked(true);
			httpPost.setConfig(this.getCustomConfig());
			httpPost.setEntity(inputStreamEntity);
			response = httpclient.execute(httpPost);
			if (logger.isDebugEnabled())
				logger.debug(" send http execute status is: " + response.getStatusLine());
			HttpEntity entity = response.getEntity();
			String resultXml = EntityUtils.toString(entity, charset);
			EntityUtils.consume(entity);
			if (logger.isDebugEnabled())
				logger.debug(" send http end ");
			return resultXml != null && !"".equals(resultXml) ? resultXml : "";
		} catch (Exception e) {
			logger.error("sendHttpPost exception:", e);
			throw new HttpClientException(" sendHttpPost exception:", e);
		} finally {
			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					logger.error("sendHttpPost response close IOException:", e);
				}
			}
			if (httpclient != null) {
				try {
					httpclient.close();
				} catch (IOException e) {
					logger.error("sendHttpPost httpclient close IOException:", e);
				}
			}
		}
	}

	/**
	 * 发送httpPost请求，请求内容为序列化对象
	 * 
	 * @param url
	 *            请求地址
	 * @param charset
	 *            响应字符编码
	 * @param serializable
	 *            序列化对象
	 * @return 响应结果
	 */
	public String sendHttpPost(String url, Charset charset, Serializable serializable) throws HttpClientException {
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		try {
			if (logger.isDebugEnabled())
				logger.debug(" send http start ");
			if (StringUtils.isBlank(url))
				throw new HttpClientException(" sendHttpPost exception,url is blank.");
			if (charset == null)
				charset = Charset.forName("UTF-8");
			httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url);
			SerializableEntity serializableEntity = new SerializableEntity(serializable);
			httpPost.setConfig(this.getCustomConfig());
			httpPost.setEntity(serializableEntity);
			response = httpclient.execute(httpPost);
			if (logger.isDebugEnabled())
				logger.debug(" send http execute status is: " + response.getStatusLine());
			HttpEntity entity = response.getEntity();
			String resultXml = EntityUtils.toString(entity, charset);
			EntityUtils.consume(entity);
			if (logger.isDebugEnabled())
				logger.debug(" send http end ");
			return resultXml != null && !"".equals(resultXml) ? resultXml : "";
		} catch (Exception e) {
			logger.error("sendHttpPost exception:", e);
			throw new HttpClientException(" sendHttpPost exception:", e);
		} finally {
			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					logger.error("sendHttpPost response close IOException:", e);
				}
			}
			if (httpclient != null) {
				try {
					httpclient.close();
				} catch (IOException e) {
					logger.error("sendHttpPost httpclient close IOException:", e);
				}
			}
		}
	}

	// 获取自定义请求配置
	private RequestConfig getCustomConfig() {
		// 三个参数依次为 请求超时时间 链接超时时间 数据响应超时时间
		RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000)
				.setSocketTimeout(10000).build();
		return requestConfig;
	}

	/**
	 * 将request请求中的参数转换字符串
	 * 
	 * @param request
	 *            请求源
	 * @param encode
	 *            字符编码
	 * @return String
	 * @throws IOException
	 */
	public String getJsonStrFromRequest(HttpServletRequest request, String encode) {
		StringBuffer buffer = new StringBuffer();
		try {
			request.setCharacterEncoding(encode);
			BufferedReader br = request.getReader();
			String temp = null;
			while ((temp = br.readLine()) != null) {
				buffer.append(temp);
				buffer.append("\n");
				if (buffer.length() > MAX_FETCHSIZE) {
					break;
				}
			}
		} catch (UnsupportedEncodingException e) {
			logger.error(" getJsonStrFromRequest UnsupportedEncodingException:", e);
			return null;
		} catch (IOException e) {
			logger.error("getJsonStrFromRequest IOException", e);
			return null;
		} catch (Exception e) {
			logger.error("getJsonStrFromRequest Exception", e);
			return null;
		}
		return buffer.toString();
	}

	public String doBillWebService(String param, String url) {
		String responseXML = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		try {
			// 创建URL
			URL urlString = new URL(url);
			URLConnection urlConn = urlString.openConnection();
			urlConn.setRequestProperty("content-type", "text/xml;charset=utf-8");
			urlConn.setDoOutput(true);
			urlConn.setReadTimeout(1200000);
			PrintWriter out = new PrintWriter(urlConn.getOutputStream());
			out.print(param);
			out.close();
			urlConn.connect();
			/* 获取服务器端返回信息 */
			isr = new InputStreamReader(urlConn.getInputStream(), "utf-8"); // 解决乱码错配合61行
			StringBuffer sb = new StringBuffer();
			if (isr != null) {
				br = new BufferedReader(isr);
				String inputLine = "";
				while ((inputLine = br.readLine()) != null) {
					sb.append(inputLine);
				}
			}
			responseXML = new String(sb.toString().getBytes());
		} catch (MalformedURLException e) {
			logger.error("doBillWebService MalformedURLException:", e);
		} catch (UnsupportedEncodingException e) {
			logger.error("doBillWebService UnsupportedEncodingException:", e);
		} catch (IOException e) {
			logger.error("doBillWebService IOException:", e);
		} finally {
			try {
				if (br != null) {
					br.close();
				}
				if (isr != null) {
					isr.close();
				}
			} catch (IOException e) {
				br = null;
				isr = null;
				logger.error("doBillWebService IOException:", e);
			}
		}
		return responseXML;
	}
	
	
	
	
	public String sendBaofoo(String url, Map<String, String> map) throws HttpClientException {
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		try {
			if (logger.isDebugEnabled())
				logger.debug(" send http start ");

			Charset charset = Charset.forName("UTF-8");
			SSLContext sslContext = SSLContexts.createSystemDefault();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
					SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			Registry<ConnectionSocketFactory> r = RegistryBuilder.<ConnectionSocketFactory>create()
					.register("https", sslsf).build();

			HttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(r);
			httpclient = HttpClients.custom().setConnectionManager(cm).build();
			HttpPost httpPost = new HttpPost(url.trim());

			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			for (Entry<String, String> entry : map.entrySet())
				nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nvps, charset);

			// 三个参数依次为 请求超时时间 链接超时时间 数据响应超时时间
			RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(10000)
					.setConnectTimeout(10000).setSocketTimeout(10000).build();
			httpPost.setConfig(requestConfig);
			httpPost.setEntity(urlEncodedFormEntity);
			// 执行请求，得到响应对象
			response = httpclient.execute(httpPost);
			if (logger.isDebugEnabled())
				logger.debug(" send http execute status is: " + response.getStatusLine());
			// 得到响应实体
			HttpEntity entity = response.getEntity();
			// 转换成字符串
			String resultXml = EntityUtils.toString(entity, charset);
			// 检查是否读取完毕
			EntityUtils.consume(entity);
			if (logger.isDebugEnabled())
				logger.debug(" send http end ");
			return resultXml != null && !"".equals(resultXml) ? resultXml : "";
		} catch (Exception e) {
			logger.error("sendHttpPost exception:", e);
			throw new HttpClientException(" sendHttpPost exception: ", e);
		} finally {
			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					logger.error("sendHttpPost response close IOException:", e);
				}
			}
			if (httpclient != null) {
				try {
					httpclient.close();
				} catch (IOException e) {
					logger.error("sendHttpPost httpclient close IOException:", e);
				}
			}
		}
	}

}
