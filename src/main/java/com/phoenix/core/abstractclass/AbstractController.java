/*
 *  @Title AbstractController.java
 *  @Package： com.phoenix.core.abstractclass
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.abstractclass;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.phoenix.core.logger.SimpleLogger;

/**
 *  @ClassName AbstractController
 *  @Description 抽象做消息处理公共操作
 *  @author liuwenbin
 *  @version 1.0
 *  @date 2017年7月6日
 */
public abstract class AbstractController {

	protected String userId;// 用户ID

	protected String userLoginName;// 用户登录名

	protected String userEmail;// 用户邮箱

	protected String userMobile;// 用户手机

	protected String userNickName;// 用户昵称

	protected String userType;// 用户类型

	protected final Lock lock = new ReentrantLock();

	// 子类可以不用写这个了,直接用就可以
	protected SimpleLogger logger = SimpleLogger.getLogger(this.getClass());

}
