/*
 *  @Title Number.java
 *  @Package： com.phoenix.core.annotation
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.phoenix.core.annotation.impl.NumberArrayValidator;
import com.phoenix.core.annotation.impl.NumberValidator;

/**
 *  @ClassName Number
 *  @Description 类注释
 *  @author liuwei
 *  @version 1.0
 *  @date 2017年7月28日
 */
@Documented
@Constraint(validatedBy = { NumberValidator.class, NumberArrayValidator.class })
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
public @interface Number {

	String message() default "{constraints.default.Number.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String filed() default "";

}
