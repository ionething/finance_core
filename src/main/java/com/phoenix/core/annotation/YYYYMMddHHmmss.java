/*
 *  @Title YYYYMMddHHmmss.java
 *  @Package： com.phoenix.core.annotation
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 *  @ClassName YYYYMMddHHmmss
 *  @Description 类注释
 *  @author liuwenbin
 *  @version 1.0
 *  @date 2017年4月26日
 */
@Documented
@Target({ FIELD })
@Retention(RUNTIME)
public @interface YYYYMMddHHmmss {

}
