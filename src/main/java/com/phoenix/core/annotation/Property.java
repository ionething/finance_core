/*
 *  @Title Property.java
 *  @Package： com.phoenix.core.annotation
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *  @ClassName Property
 *  @Description 类注释
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年7月3日
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
public @interface Property {

	String name() default "";
}
