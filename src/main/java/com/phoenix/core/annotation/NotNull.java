/*
 *  @Title NotNull.java
 *  @Package： com.phoenix.core.annotation
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Payload;

/**
 *  @ClassName NotNull
 *  @Description 类注释
 *  @author yijun
 *  @version 1.0
 *  @date 2017年5月7日
 */
@Documented
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
@NotNull
public @interface NotNull {

	String message() default "{constraints.default.NotNull.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String filed() default "";

}
