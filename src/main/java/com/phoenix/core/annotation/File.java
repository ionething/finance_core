/*
 *  @Title File.java
 *  @Package： com.phoenix.core.annotation
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.phoenix.core.annotation.impl.FileValidator;

/**
 *  @ClassName File
 *  @Description 上传文件验证
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年6月11日
 */
@Documented
@Constraint(validatedBy = FileValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
public @interface File {

	String message() default "{constraints.default.File.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String filed() default "";

	boolean isValidateTypes() default true;

	boolean isValidateEmpty() default true;

	long size() default 5242880;

	String types() default "image/jpeg image/bmp image/gif image/png image/pjpeg image/x-png";

}
