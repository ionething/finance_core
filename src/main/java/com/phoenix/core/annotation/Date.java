/*
 *  @Title Date.java
 *  @Package： com.phoenix.core.annotation
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.phoenix.core.annotation.impl.DateArrayValidator;
import com.phoenix.core.annotation.impl.DateValidator;

/**
 *  @ClassName Date
 *  @Description 类注释
 *  @author liuwenbin
 *  @version 1.0
 *  @date 2017年5月26日
 */
@Documented
@Constraint(validatedBy = { DateValidator.class, DateArrayValidator.class })
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
public @interface Date {

	String message() default "{constraints.default.Date.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String format() default "yyyy-MM-dd";

	String filed() default "";

}
