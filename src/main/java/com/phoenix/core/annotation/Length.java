/*
 *  @Title Length.java
 *  @Package： com.phoenix.core.annotation
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.phoenix.core.annotation.impl.LengthArrayValidator;
import com.phoenix.core.annotation.impl.LengthValidator;

/**
 *  @ClassName Length
 *  @Description 类注释
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年6月18日
 */
@Documented
@Constraint(validatedBy = { LengthValidator.class, LengthArrayValidator.class })
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
public @interface Length {

	int min() default 0;

	int max() default Integer.MAX_VALUE;

	boolean isEdit() default false; // 是否为富文本编辑器

	String filed() default "";

	String message() default "{constraints.default.Langth.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
