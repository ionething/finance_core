/*
 *  @Title NumberArrayValidator.java
 *  @Package： com.phoenix.core.annotation.impl
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.annotation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import com.phoenix.core.annotation.Number;

/**
 *  @ClassName NumberArrayValidator
 *  @Description 类注释
 *  @author yijun
 *  @version 1.0
 *  @date 2017年5月6日
 */
public class NumberArrayValidator implements ConstraintValidator<Number, String[]> {

	@Override
	public void initialize(Number constraintAnnotation) {

	}

	@Override
	public boolean isValid(String[] value, ConstraintValidatorContext context) {
		if (value == null || value.length == 0) {
			return true;
		}
		for (String v : value) {
			return StringUtils.isNumeric(v);
		}
		return true;
	}

}
