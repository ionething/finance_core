/*
 *  @Title DateValidator.java
 *  @Package： com.phoenix.core.annotation.impl
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.annotation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import com.phoenix.core.annotation.Date;
import com.phoenix.core.util.DateUtils;

/**
 *  @ClassName DateValidator
 *  @Description 日期注解验证实现
 *  @author yijun
 *  @version 1.0
 *  @date 2017年4月23日
 */
public class DateValidator implements ConstraintValidator<Date, String> {

	private String format;

	@Override
	public void initialize(Date constraintAnnotation) {
		format = constraintAnnotation.format();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext constraint) {
		if (StringUtils.isEmpty(value)) {
			return Boolean.TRUE;
		}
		return DateUtils.isValidDate(value, format);
	}
}
