/*
 *  @Title JcTreeNode.java
 *  @Package： com.phoenix.core.tree
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.tree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *  @ClassName JcTreeNode
 *  @Description 树节点实现
 *  @author yijun
 *  @version 1.0
 *  @date 2017年5月5日
 */
public class JcTreeNode extends AbstractNode {

	public JcTreeNode() {
	}

	/** 将节点设置为虚拟根节点 */
	public void serRoot() {
		super.setRoot();
	}

	public JcTreeNode(TreeNode node) {
		super(node);
	}

	/** 节点的孩子节点集合 */
	private List<TreeNode> childs = new ArrayList<TreeNode>();

	public void addNode(TreeNode node) {
		childs.add(node);
	}

	/** 孩子节点迭代器 */
	public Iterator<TreeNode> iterator() {
		return childs.iterator();
	}

	/** 节点的孩子节点集合大小 */
	public int getSize() {
		return childs.size();
	}

	/** 返回节点的孩子节点集合 */
	public List<TreeNode> getList() {
		return childs;
	}

	public List<TreeNode> getChilds() {
		return childs;
	}

	public void setChilds(List<TreeNode> childs) {
		childs = null;
		this.childs = childs;
	}

	public void setList(List<TreeNode> list) {
		childs = null;
		childs = list;
	}

}
