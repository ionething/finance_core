/*
 *  @Title BreadthIterator.java
 *  @Package： com.phoenix.core.tree
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.tree;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 *  @ClassName BreadthIterator
 *  @Description 普通树的广度遍历方法
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年4月21日
 */
public class BreadthIterator implements Iterator<TreeNode> {

	/** 迭代器队列 */
	private Queue<Iterator<TreeNode>> queue = new LinkedList<Iterator<TreeNode>>();

	public BreadthIterator(Iterator<TreeNode> it) {
		queue.offer(it);
	}

	/** 是否存在下个元素,没有则返回同级元素的迭代器遍历 */
	public boolean hasNext() {
		if (queue.isEmpty()) {
			return Boolean.FALSE;
		} else {
			Iterator<TreeNode> it = queue.peek();
			if (it.hasNext()) {
				return Boolean.TRUE;
			} else {
				queue.poll();
				return hasNext();
			}
		}
	}

	/** 返回下个元素,无则返回<tt>null</tt> */
	public TreeNode next() {
		if (hasNext()) {
			Iterator<TreeNode> it = queue.peek();
			TreeNode next = it.next();
			if (next.getSize() > 0) {
				queue.offer(next.iterator());
			}
			return next;
		} else {
			return null;
		}
	}

	/** 不提供删除方法 */
	public void remove() {
		throw new UnsupportedOperationException("Can't remove node :KinjoYang");
	}
}
