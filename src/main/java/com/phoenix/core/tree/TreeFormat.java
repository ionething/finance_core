/*
 *  @Title TreeFormat.java
 *  @Package： com.phoenix.core.tree
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.tree;

import java.util.Map;

/**
 *  @ClassName TreeFormat
 *  @Description 内存树转换接口
 *  @author liuwei
 *  @version 1.0
 *  @date 2017年7月21日
 */
public interface TreeFormat {

	/**
	 * @param node
	 *            父节点
	 * @param nodeStr
	 *            父节点数据
	 * @param childs
	 *            孩子节点数据
	 * @return StringBuffer 父节点与孩子节点组合
	 */
	public StringBuffer getBranchNode(TreeNode node, String nodeStr, String childs);

	/**
	 * @param node
	 *            节点数据
	 * @return StringBuffer 返回叶子节点的格式化数据
	 */
	public StringBuffer getLeafNode(TreeNode node);

	/**
	 * 根据内存树格式化
	 * 
	 * @param treeNode
	 *            根节点
	 * @param treeMp
	 *            树节点map
	 * @return StringBuffer 格式化后的树
	 */
	public StringBuffer treeDataFormat(TreeNode treeNode, Map<String, TreeNode> treeMp);

	/**
	 * 根据内存树格式化
	 * 
	 * @param treeNode
	 *            根节点
	 * @param treeMp
	 *            树节点map
	 * @return StringBuffer 格式化后的树
	 */
	public StringBuffer treeDataFormat(TreeNode treeNode, Map<String, TreeNode> treeMp, String condition, int type);

	/**
	 * @param node
	 *            节点数据
	 * @return StringBuffer 返回叶子节点的格式化数据,为菜单提供的方法
	 */
	public StringBuffer getMenuLeafNode(TreeNode node);

}
