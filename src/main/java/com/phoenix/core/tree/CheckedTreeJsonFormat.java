/*
 *  @Title CheckedTreeJsonFormat.java
 *  @Package： com.phoenix.core.tree
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.tree;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

/**
 *  @ClassName CheckedTreeJsonFormat
 *  @Description 内存树转换带的树的格式数据
 *  @author yijun
 *  @version 1.0
 *  @date 2017年5月6日
 */
@Service("checkedTreeformat")
public class CheckedTreeJsonFormat implements TreeFormat {

	protected final String jbegin = "{", jend = "}", jsbegin = "\"children\":[", jsend = "]", jsplit = ",",
			idstr = "\"id\":", textstr = "\"text\":", check = "\"checked\":true", nocheck = "\"checked\":false",
			leafstr = "\"leaf\":";

	protected Map<String, Integer> counstack = null;

	protected JcStack<Iterator<TreeNode>> treestack = null;

	protected Map<String, StringBuffer> treeBuffer = null;

	protected JcStack<TreeNode> loststack = null;

	protected Map<String, Boolean> had = null;

	/** 得到叶子节点的json格式数据 */
	public StringBuffer getLeafNode(TreeNode node) {
		StringBuffer jsonbf = new StringBuffer(50);
		jsonbf.append(jbegin);
		jsonbf.append(idstr);
		jsonbf.append("\"" + node.getId() + "\"");
		jsonbf.append(jsplit);
		jsonbf.append(textstr);
		jsonbf.append("\"" + node.getNodeValue() + "\"");
		jsonbf.append(jsplit);
		if (node.isChecked()) {
			jsonbf.append(check);
		} else {
			jsonbf.append(nocheck);
		}
		jsonbf.append(jsplit);
		jsonbf.append(leafstr);
		jsonbf.append(node.getSize() == 0);
		jsonbf.append(jend);
		return jsonbf;
	}

	public StringBuffer getCustomLeafNode(TreeNode node, String str) {
		StringBuffer jsonbf = new StringBuffer(50);
		jsonbf.append(jbegin);
		jsonbf.append(idstr);
		jsonbf.append("\"" + node.getId() + "\"");
		jsonbf.append(jsplit);
		jsonbf.append(textstr);
		jsonbf.append("\"" + node.getNodeValue() + "\"");
		jsonbf.append(jsplit);
		jsonbf.append(str);
		jsonbf.append(jsplit);
		if (node.isChecked()) {
			jsonbf.append(check);
		} else {
			jsonbf.append(nocheck);
		}
		jsonbf.append(jsplit);
		jsonbf.append(leafstr);
		jsonbf.append(node.getSize() == 0);
		jsonbf.append(jend);
		return jsonbf;
	}

	/** 得到树枝节点的json格式数据 */
	public StringBuffer getBranchNode(TreeNode node, String nodeStr, String childs) {
		StringBuffer jsonbf = new StringBuffer();
		if (childs == null || childs.equals("")) {
			jsonbf = getLeafNode(node);
			return jsonbf;
		}
		if (nodeStr.indexOf(jsbegin) > 0) {
			String tempchild = nodeStr.substring(nodeStr.indexOf(jsbegin) + 12, nodeStr.lastIndexOf(jsend));
			jsonbf.append(jbegin);
			jsonbf.append(idstr);
			jsonbf.append("\"" + node.getId() + "\"");
			jsonbf.append(jsplit);
			jsonbf.append(textstr);
			jsonbf.append("\"" + node.getNodeValue() + "\"");
			jsonbf.append(jsplit);
			if (node.isChecked()) {
				jsonbf.append(check);
			} else {
				jsonbf.append(nocheck);
			}
			jsonbf.append(jsplit);
			jsonbf.append(jsbegin);
			jsonbf.append(tempchild);
			jsonbf.append(jsplit);
			jsonbf.append(childs);
			jsonbf.append(jsend);
			jsonbf.append(jend);
		} else {
			jsonbf.append(jbegin);
			jsonbf.append(idstr);
			jsonbf.append("\"" + node.getId() + "\"");
			jsonbf.append(jsplit);
			jsonbf.append(textstr);
			jsonbf.append("\"" + node.getNodeValue() + "\"");
			jsonbf.append(jsplit);
			if (node.isChecked()) {
				jsonbf.append(check);
			} else {
				jsonbf.append(nocheck);
			}
			jsonbf.append(jsplit);
			jsonbf.append(jsbegin);
			jsonbf.append(childs);
			jsonbf.append(jsend);
			jsonbf.append(jend);
		}
		return jsonbf;
	}

	/**
	 * 根据传入内存树格式化
	 */
	synchronized public StringBuffer treeDataFormat(TreeNode treeNode, Map<String, TreeNode> treeMp) {
		if (treeMp.isEmpty())
			return new StringBuffer();
		treestack = new JcStack<Iterator<TreeNode>>();
		counstack = new HashMap<String, Integer>();
		loststack = new JcStack<TreeNode>();
		treeBuffer = new HashMap<String, StringBuffer>();
		had = new HashMap<String, Boolean>();
		treestack.push(treeNode.iterator());
		loststack.push(treeNode);
		treeBuffer.put(treeNode.getId(), getLeafNode(treeNode));
		int i = 0;
		TreeNode pa = null;
		while (printhasNext()) {
			i++;
			Iterator<TreeNode> it = treestack.peek();
			TreeNode next = it.next();
			treeBuffer.put(next.getId(), getLeafNode(next));
			counstack.put(next.getId(), next.getSize());
			if (next.getSize() > 0) {
				treestack.push(next.iterator());
				loststack.push(next);
				i = 0;
			} else {
				Integer ii = counstack.get(next.getParentId());
				if (ii != null) {
					if (i == ii.intValue()) {
						pa = treeMp.get(next.getParentId());
						getParent(pa, pa.getList());
						i = 1;
					}
				}
			}
		}
		getLost();
		StringBuffer rtn = treeBuffer.get(treeNode.getId());
		treestack = null;
		counstack = null;
		loststack = null;
		treeBuffer = null;
		had = null;
		return rtn == null ? new StringBuffer() : rtn;
	}

	protected void getParent(TreeNode parent, List<TreeNode> list) {
		StringBuffer temppaBuffer = null;
		StringBuffer paBuffer = null;
		had.put(parent.getId(), Boolean.TRUE);
		for (TreeNode tree : list) {
			temppaBuffer = treeBuffer.get(parent.getId());
			if (temppaBuffer == null) {
				continue;
			}
			if (treeBuffer.get(tree.getId()) == null) {
				continue;
			}
			paBuffer = getBranchNode(parent, temppaBuffer.toString(), treeBuffer.get(tree.getId()).toString());
			treeBuffer.put(parent.getId(), paBuffer);
		}
	}

	protected void getLost() {
		TreeNode lost = null;
		Boolean flag = null;
		while (loststack.isEmpty() == false) {
			lost = loststack.pop();
			flag = had.get(lost.getId());
			if (flag != null) {
				continue;
			}
			getParent(lost, lost.getList());
		}
	}

	protected boolean printhasNext() {
		if (treestack.isEmpty()) {
			return false;
		} else {
			Iterator<TreeNode> it = treestack.peek();
			if (it.hasNext()) {
				return true;
			} else {
				treestack.pop();
				return printhasNext();
			}
		}
	}

	public StringBuffer treeDataFormat(TreeNode treeNode, Map<String, TreeNode> treeMp, String condition, int type) {
		return new StringBuffer();
	}

	@Override
	public StringBuffer getMenuLeafNode(TreeNode node) {
		// 
		return null;
	}

}
