/*
 *  @Title NullIterator.java
 *  @Package： com.phoenix.core.tree
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.tree;

import java.util.Iterator;

/**
 *  @ClassName NullIterator
 *  @Description 空迭代器
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年4月27日
 */
public class NullIterator<E> implements Iterator<E> {

	/** 永远返回false */
	public boolean hasNext() {
		return Boolean.FALSE;
	}

	/** 下个元素为null */
	public E next() {
		return null;
	}

	/** 不提供删除方法 */
	public void remove() {
		throw new UnsupportedOperationException("Can't remove obj:KinjoYang");
	}
}
