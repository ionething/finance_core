/*
 *  @Title AbstractNode.java
 *  @Package： com.phoenix.core.tree
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.tree;

/**
 *  @ClassName AbstractNode
 *  @Description 树的抽象节点
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年6月19日
 */
public abstract class AbstractNode implements TreeNode {

	/** 节点ID */
	protected String Id;

	/** 父节点ID */
	protected String parentId;

	/** 节点名称 */
	protected String nodeName;

	/** 节点值 */
	protected String nodeValue;

	/** 节点值 */
	protected boolean isChecked = false;

	/** 节点链接 */
	protected String link;

	/** 节点代号 */
	protected String tcode;

	/** 节点图标 */
	protected String icon;

	/** 节点优先级 */
	protected int priority;

	public AbstractNode() {
	}

	/**
	 * 将节点设置为虚拟根节点
	 */
	public void setRoot() {
		this.Id = "kinjo";
		this.parentId = "";
		this.nodeName = "kinjo";
		this.nodeValue = "kinjo";
	}

	protected AbstractNode(TreeNode node) {
		this.Id = node.getId();
		this.parentId = node.getParentId();
		this.nodeName = node.getNodeName();
		this.nodeValue = node.getNodeValue();
		this.icon = node.getIcon();
		this.isChecked = node.isChecked();
	}

	public String getId() {
		return Id;
	}

	public void setId(String keyId) {
		this.Id = keyId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getNodeValue() {
		return nodeValue;
	}

	public void setNodeValue(String nodeValue) {
		this.nodeValue = nodeValue;
	}

	public final boolean isChecked() {
		return isChecked;
	}

	public final void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getTcode() {
		return tcode;
	}

	public void setTcode(String tcode) {
		this.tcode = tcode;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

}
