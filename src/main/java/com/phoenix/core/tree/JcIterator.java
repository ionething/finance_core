/*
 *  @Title JcIterator.java
 *  @Package： com.phoenix.core.tree
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.tree;

import java.util.Iterator;

/**
 *  @ClassName JcIterator
 *  @Description 迭代器实现类
 *  @author liuwb
 *  @version 1.0
 *  @date 2017年5月27日
 */
public class JcIterator implements Iterator<TreeNode> {

	/** 迭代器栈 */
	private JcStack<TreeNode> jcIterator = new JcStack<TreeNode>();

	public JcIterator(Iterator<TreeNode> ittree) {
		while (ittree.hasNext()) {
			jcIterator.push(ittree.next());
		}
	}

	/** 栈是否完毕 */
	public boolean hasNext() {
		if (jcIterator.isEmpty()) {
			return Boolean.FALSE;
		} else {
			return Boolean.TRUE;
		}
	}

	/** 返回下个元素,不弹出栈 */
	public TreeNode find() {
		return jcIterator.peek();
	}

	/** 返回下个元素,同时弹出栈 */
	public TreeNode next() {
		return jcIterator.pop();
	}

	/** 不提供删除方法 */
	public void remove() {
		throw new UnsupportedOperationException("Can't remove obj:Kinjo");
	}

}
