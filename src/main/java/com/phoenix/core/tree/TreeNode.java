/*
 *  @Title TreeNode.java
 *  @Package： com.phoenix.core.tree
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.tree;

import java.util.Iterator;
import java.util.List;

/**
 *  @ClassName TreeNode
 *  @Description 树节点接口
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年4月23日
 */
public interface TreeNode {

	/** 增加子节点 */
	void addNode(TreeNode treenode);

	/** 子节点迭代器 */
	Iterator<TreeNode> iterator();

	/** 返回节点ID */
	String getId();

	/** 返回父节点ID */
	String getParentId();

	/** 返回节点名称 */
	String getNodeName();

	/** 返回节点值 */
	String getNodeValue();

	/** 得到link */
	String getLink();

	/** 得到图标样式 */
	String getIcon();

	/** 返回子节点集合大小 */
	int getSize();

	/** 优先级 */
	int getPriority();

	/** 将节点设置为根节点(注意实际根节点的父节点必须为空,不能为自己节点ID) */
	void setRoot();

	/** 节点是否被选中 */
	boolean isChecked();

	void setChecked(boolean flag);

	void setNodeValue(String value);

	/** 返回子节点集合 */
	List<TreeNode> getList();

	/** 设置子节点集合 */
	void setList(List<TreeNode> list);

}
