/*
 *  @Title JcStack.java
 *  @Package： com.phoenix.core.tree
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.tree;

import java.util.LinkedList;

/**
 *  @ClassName JcStack
 *  @Description 实现堆栈功能如果只是需要栈功能可以用此类比真正的栈瘦
 *  @author yijun
 *  @version 1.0
 *  @date 2017年4月25日
 */
public class JcStack<E> {

	/** 栈链表 */
	private LinkedList<E> stacklist = new LinkedList<E>();

	/** 栈压入元素 */
	public void push(E e) {
		stacklist.addFirst(e);
	}

	/** 栈首元素 */
	public E top() {
		return stacklist.getFirst();
	}

	/** 栈弹出元素 */
	public E pop() {
		return stacklist.removeFirst();
	}

	/** 栈返回元素,但不弹出 */
	public E peek() {
		return stacklist.getFirst();
	}

	/** 栈大小 */
	public int getSize() {
		return stacklist.size();
	}

	/** 栈是否为空 */
	public boolean isEmpty() {
		return stacklist.size() == 0;
	}
}
