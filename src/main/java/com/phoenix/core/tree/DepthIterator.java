/*
 *  @Title DepthIterator.java
 *  @Package： com.phoenix.core.tree
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.tree;

import java.util.Iterator;

/**
 *  @ClassName DepthIterator
 *  @Description 普通树的深度遍历方法
 *  @author liuwb
 *  @version 1.0
 *  @date 2017年7月13日
 */
public class DepthIterator implements Iterator<TreeNode> {

	/** 迭代器栈 */
	private JcStack<Iterator<TreeNode>> stack = new JcStack<Iterator<TreeNode>>();

	public DepthIterator(Iterator<TreeNode> it) {
		stack.push(it);
	}

	public DepthIterator() {
	}

	/** 是否存在下个元素,没有则返回下级元素的迭代器遍历 */
	public boolean hasNext() {
		if (stack.isEmpty()) {
			return Boolean.FALSE;
		} else {
			Iterator<TreeNode> it = stack.peek();
			if (it.hasNext()) {
				return Boolean.TRUE;
			} else {
				stack.pop();
				return hasNext();
			}
		}
	}

	/** 返回下个元素,无则返回<tt>null</tt> */
	public TreeNode next() {
		if (hasNext()) {
			Iterator<TreeNode> it = stack.peek();
			TreeNode next = it.next();
			if (next.getSize() > 0) {
				stack.push(next.iterator());
			}
			return next;
		} else {
			return null;
		}
	}

	/** 不提供删除方法 */
	public void remove() {
		throw new UnsupportedOperationException("Can't remove node :KinjoYang");
	}

}
