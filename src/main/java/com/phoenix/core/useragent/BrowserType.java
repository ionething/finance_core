/*
 *  @Title BrowserType.java
 *  @Package： com.phoenix.core.useragent
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.useragent;

/**
 *  @ClassName BrowserType
 *  @Description 类注释
 *  @author yijun
 *  @version 1.0
 *  @date 2017年4月19日
 */
public enum BrowserType {

	/**
	 * Standard web-browser
	 */
	WEB_BROWSER("Web浏览器"),
	/**
	 * Special web-browser for mobile devices
	 */
	MOBILE_BROWSER("移动端浏览器"),
	/**
	 * Text only browser like the good old Lynx
	 */
	TEXT_BROWSER("Browser(text only)"),
	/**
	 * Email client like Thunderbird
	 */
	EMAIL_CLIENT("Email Client"),
	/**
	 * Search robot, spider, crawler,...
	 */
	ROBOT("Robot"),
	/**
	 * Downloading tools
	 */
	TOOL("Downloading tool"), UNKNOWN("unknown");

	private String name;

	private BrowserType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
