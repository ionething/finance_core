/*
 *  @Title RenderingEngine.java
 *  @Package： com.phoenix.core.useragent
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.useragent;

/**
 *  @ClassName RenderingEngine
 *  @Description 类注释
 *  @author yijun
 *  @version 1.0
 *  @date 2017年6月15日
 */
public enum RenderingEngine {

	/**
	 * Trident is the the Microsoft layout engine, mainly used by Internet
	 * Explorer.
	 */
	TRIDENT("Trident"),
	/**
	 * HTML parsing and rendering engine of Microsoft Office Word, used by some
	 * other products of the Office suite instead of Trident.
	 */
	WORD("Microsoft Office Word"),
	/**
	 * Open source and cross platform layout engine, used by Firefox and many
	 * other browsers.
	 */
	GECKO("Gecko"),
	/**
	 * Layout engine based on KHTML, used by Safari, Chrome and some other
	 * browsers.
	 */
	WEBKIT("WebKit"),
	/**
	 * Proprietary layout engine by Opera Software ASA
	 */
	PRESTO("Presto"),
	/**
	 * Original layout engine of the Mozilla browser and related products.
	 * Predecessor of Gecko.
	 */
	MOZILLA("Mozilla"),
	/**
	 * Layout engine of the KDE project
	 */
	KHTML("KHTML"),
	/**
	 * Other or unknown layout engine.
	 */
	OTHER("Other");

	String name;

	private RenderingEngine(String name) {
		this.name = name;
	}

}
