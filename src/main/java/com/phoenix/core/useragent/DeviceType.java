/*
 *  @Title DeviceType.java
 *  @Package： com.phoenix.core.useragent
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.useragent;

/**
 *  @ClassName DeviceType
 *  @Description 类注释
 *  @author yijun
 *  @version 1.0
 *  @date 2017年4月20日
 */
public enum DeviceType {

	/**
	 * Standard desktop or laptop computer
	 */
	COMPUTER("PC电脑"),
	/**
	 * Mobile phone or similar small mobile device
	 */
	MOBILE("手机"),
	/**
	 * Small tablet type computer.
	 */
	TABLET("平板"),
	/**
	 * Game console like the Wii or Playstation.
	 */
	GAME_CONSOLE("Game console"),
	/**
	 * Digital media receiver like the Apple TV. No device detection implemented
	 * yet for this category. Please send provide user-agent strings if you have
	 * some.
	 */
	DMR("Digital media receiver"),
	/**
	 * Other or unknow type of device.
	 */
	UNKNOWN("未知设备");

	String name;

	private DeviceType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
