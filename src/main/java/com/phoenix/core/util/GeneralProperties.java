/*
 *  @Title GeneralProperties.java
 *  @Package： com.phoenix.core.util
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import com.phoenix.core.logger.SimpleLogger;

/**
 *  @ClassName GeneralProperties
 *  @Description 属性文件读取
 *  @author liuwb
 *  @version 1.0
 *  @date 2017年5月21日
 */
public class GeneralProperties extends PropertyPlaceholderConfigurer {


	static PropertiesHelper propsHelper;
	private static Properties props;

	public Properties mergeProperties() {
		try {
			props = super.mergeProperties();
		} catch (IOException e) {

		}
		return props;
	}

	static SimpleLogger _log = SimpleLogger.getLogger(GeneralProperties.class.getClass());

	private GeneralProperties() {
	}

	private static void loadProperties() {
		propsHelper = new PropertiesHelper(props);
	}

	public static Properties getProperties() {
		return getHelper().getProperties();
	}

	private static PropertiesHelper getHelper() {
		if (propsHelper == null)
			loadProperties();
		return propsHelper;
	}

	public static String getProperty(String key, String defaultValue) {
		return getHelper().getProperty(key, defaultValue);
	}

	public static String getProperty(String key) {
		return getHelper().getProperty(key);
	}

	public static String getRequiredProperty(String key) {
		return getHelper().getRequiredProperty(key);
	}

	public static int getRequiredInt(String key) {
		return getHelper().getRequiredInt(key);
	}

	public static boolean getRequiredBoolean(String key) {
		return getHelper().getRequiredBoolean(key);
	}

	public static String getNullIfBlank(String key) {
		return getHelper().getNullIfBlank(key);
	}

	public static void setProperty(String key, String value) {
		getHelper().setProperty(key, value);
	}

	public static Properties loadAllPropertiesByClassLoader(String resourceName) throws IOException {
		Properties properties = new Properties();
		InputStream inputStream = GeneralProperties.class.getClassLoader().getResourceAsStream(resourceName);
		if (inputStream != null) {
			properties.load(inputStream);
		}
		return properties;
	}
}
