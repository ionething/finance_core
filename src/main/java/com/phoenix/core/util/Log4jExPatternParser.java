/*
 *  @Title Log4jExPatternParser.java
 *  @Package： com.phoenix.core.util
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.util;

import org.apache.log4j.helpers.FormattingInfo;
import org.apache.log4j.helpers.PatternConverter;
import org.apache.log4j.helpers.PatternParser;
import org.apache.log4j.spi.LoggingEvent;

/**
 *  @ClassName Log4jExPatternParser
 *  @Description 类注释
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年7月27日
 */
public class Log4jExPatternParser extends PatternParser { 
	  
    public Log4jExPatternParser(String pattern) { 
        super(pattern); 
    } 
     /** 
      * 重写finalizeConverter，对特定的占位符进行处理，T表示线程ID占位符 
      */
     @Override
     protected void finalizeConverter(char c) { 
      if (c == 'T') { 
       this.addConverter(new ExPatternConverter(this.formattingInfo)); 
      } else { 
       super.finalizeConverter(c); 
      } 
     } 
  
     private static class ExPatternConverter extends PatternConverter { 
  
      public ExPatternConverter(FormattingInfo fi) { 
       super(fi); 
      } 
  
      /** 
       * 当需要显示线程ID的时候，返回当前调用线程的ID 
       */
      @Override
      protected String convert(LoggingEvent event) { 
       return "线程名:"+Thread.currentThread().getName()+"___线程id:"+Thread.currentThread().getId(); 
      } 
  
     } 
  
  
}
