/*
 *  @Title JcParameterProperty.java
 *  @Package： com.phoenix.core.util
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.util;

import java.util.Map;

import org.springframework.beans.MutablePropertyValues;

/**
 *  @ClassName JcParameterProperty
 *  @Description 封装数据绑定参数
 *  @author liuwei
 *  @version 1.0
 *  @date 2017年6月10日
 */
public class JcParameterProperty extends MutablePropertyValues {

	private static final long serialVersionUID = -5184248725920634553L;

	public JcParameterProperty(Map<?, ?> original) {
		super(original);
	}
}
