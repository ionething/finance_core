/*
 *  @Title ScriptAnalyzeUtil.java
 *  @Package： com.phoenix.core.util
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.util;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.lang3.StringUtils;

import com.phoenix.core.logger.SimpleLogger;

/**
 *  @ClassName ScriptAnalyzeUtil
 *  @Description 脚本解析
 *  @author liuwb
 *  @version 1.0
 *  @date 2017年4月26日
 */
public class ScriptAnalyzeUtil {
	
	private static SimpleLogger logger = SimpleLogger.getLogger(ScriptAnalyzeUtil.class);

	/**
	 * 解析布尔字符串 如字符串为 y>=1 变量名为y 变量值为2 返回true
	 * @param text 字符串
	 * @param value 变量值
	 * @param variable 变量名
	 * @return
	 */
	public static Boolean  analyzeBoolStr(String text, Object value,String variable ){
		 ScriptEngineManager manager = new ScriptEngineManager();
	     ScriptEngine engine = manager.getEngineByName("js");
	     if(StringUtils.isNotBlank(variable)){
	    	  engine.put(variable,value);
	     }
	     try {
			return  (Boolean) engine.eval(text);
		} catch (Exception e) {
			logger.error("analyzeBoolStr exception:",e);
		}
	    return Boolean.FALSE;
	 }
	
	/**
	 * 解析布尔字符串 如:传入 1>2 返回false
	 * @param text
	 * @return
	 */
	public static Boolean analyzeBoolStr(String text){
		 ScriptEngineManager manager = new ScriptEngineManager();
	     ScriptEngine engine = manager.getEngineByName("js");
	     try {
			return  (Boolean) engine.eval(text);
		} catch (Exception e) {
			logger.error("analyzeBoolStr exception:",e);
		}
	    return Boolean.FALSE;
	 }
	
}
