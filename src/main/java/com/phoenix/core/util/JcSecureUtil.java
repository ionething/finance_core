/*
 *  @Title JcSecureUtil.java
 *  @Package： com.phoenix.core.util
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.phoenix.core.logger.SimpleLogger;

/**
 *  @ClassName JcSecureUtil
 *  @Description 加解密工具类
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年6月12日
 */
public final class JcSecureUtil {

	static SimpleLogger logger = SimpleLogger.getLogger(JcSecureUtil.class);

	static String keyWord = "jc_tzp_77_jssnhj";

	private static final byte[] ROW_BYTES = "80e36e3f934e678c".getBytes();

	public static Key getKey() {
		SecretKey key = null;
		try {
			DESKeySpec dks = new DESKeySpec(keyWord.getBytes("UTF-8"));
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			key = keyFactory.generateSecret(dks);
		} catch (InvalidKeySpecException e) {
			logger.error(" getKey exception : ", e);
		} catch (NoSuchAlgorithmException e) {
			logger.error(" getKey exception : ", e);
		} catch (InvalidKeyException e) {
			logger.error(" getKey exception : ", e);
		} catch (UnsupportedEncodingException e) {
			logger.error(" getKey exception : ", e);
		}
		return key;
	}

/*	public static String encode(String data) {
		if (BaseUtils.isEmpty(data)) {
			return data;
		}
		try {
			data = data.replaceAll("@", "_jc-online_");
			byte[] databytes = data.getBytes("UTF-8");
			Cipher cipher = Cipher.getInstance("DES");
			cipher.init(Cipher.ENCRYPT_MODE, getKey());
			byte[] encryptBytes = cipher.doFinal(databytes);
			return Base64.encodeBase64String(encryptBytes);
		} catch (UnsupportedEncodingException e) {
			logger.error(" encode exception : ", e);
		} catch (NoSuchAlgorithmException e) {
			logger.error(" encode exception : ", e);
		} catch (NoSuchPaddingException e) {
			logger.error(" encode exception : ", e);
		} catch (InvalidKeyException e) {
			logger.error(" encode exception : ", e);
		} catch (IllegalBlockSizeException e) {
			logger.error(" encode exception : ", e);
		} catch (BadPaddingException e) {
			logger.error(" encode exception : ", e);
		}
		return data;
	}

	public static String decode(String data) {
		if (BaseUtils.isEmpty(data)) {
			return data;
		}
		try {
			byte[] databytes = Base64.decodeBase64(data);
			Cipher cipher = Cipher.getInstance("DES");
			cipher.init(Cipher.DECRYPT_MODE, getKey());
			byte[] decryptBytes = cipher.doFinal(databytes);
			data = new String(decryptBytes, "UTF-8");
			return data.replaceAll("_zx-online_", "@");
		} catch (UnsupportedEncodingException e) {
			logger.error(" decode exception : ", e);
		} catch (NoSuchAlgorithmException e) {
			logger.error(" decode exception : ", e);
		} catch (NoSuchPaddingException e) {
			logger.error(" decode exception : ", e);
		} catch (InvalidKeyException e) {
			logger.error(" decode exception : ", e);
		} catch (IllegalBlockSizeException e) {
			logger.error(" decode exception : ", e);
		} catch (BadPaddingException e) {
			logger.error(" decode exception : ", e);
		}
		return data;
	}*/

	/**
	 * 银行卡与身份证的加密
	 * 
	 * @param cardNo
	 * @return
	 */
	public static String encodeCard(String content) {
		if (BaseUtils.isEmpty(content)) {
			return content;
		}
		try {
			Cipher cipher = Cipher.getInstance("AES");
			SecretKeySpec keySpec = new SecretKeySpec(ROW_BYTES, "AES");
			cipher.init(1, keySpec);
			byte[] ciphertext = cipher.doFinal(content.getBytes());
			return Base64.encodeBase64String(ciphertext);
		} catch (NoSuchAlgorithmException e) {
			logger.error(" encodeCard exception : ", e);
		} catch (NoSuchPaddingException e) {
			logger.error(" encodeCard exception : ", e);
		} catch (IllegalBlockSizeException e) {
			logger.error(" encodeCard exception : ", e);
		} catch (BadPaddingException e) {
			logger.error(" encodeCard exception : ", e);
		} catch (InvalidKeyException e) {
			logger.error(" encodeCard exception : ", e);
		}
		return content;
	}

	public static String decodeCard(String content) {
		if (BaseUtils.isEmpty(content)) {
			return content;
		}
		Cipher cipher;
		try {
			cipher = Cipher.getInstance("AES");
			SecretKeySpec keySpec = new SecretKeySpec(ROW_BYTES, "AES");
			cipher.init(2, keySpec);
			byte[] ciphertext = cipher.doFinal(Base64.decodeBase64(content));
			return new String(ciphertext);
		} catch (NoSuchAlgorithmException e) {
			logger.error(" decodeCard exception : ", e);
		} catch (NoSuchPaddingException e) {
			logger.error(" decodeCard exception : ", e);
		} catch (InvalidKeyException e) {
			logger.error(" decodeCard exception : ", e);
		} catch (IllegalBlockSizeException e) {
			logger.error(" decodeCard exception : ", e);
		} catch (BadPaddingException e) {
			logger.error(" decodeCard exception : ", e);
		}
		return content;
	}

}
