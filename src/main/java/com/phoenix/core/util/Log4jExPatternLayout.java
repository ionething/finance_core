/*
 *  @Title Log4jExPatternLayout.java
 *  @Package： com.phoenix.core.util
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.util;

import org.apache.log4j.PatternLayout;
import org.apache.log4j.helpers.PatternParser;

/**
 *  @ClassName Log4jExPatternLayout
 *  @Description 类注释
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年4月18日
 */
public class Log4jExPatternLayout extends PatternLayout { 
    public Log4jExPatternLayout(String pattern){ 
        super(pattern); 
    } 
      
    public Log4jExPatternLayout(){ 
        super(); 
    } 
     /** 
      * 重写createPatternParser方法，返回PatternParser的子类 
      */
     @Override
     protected PatternParser createPatternParser(String pattern) { 
      return new Log4jExPatternParser(pattern); 
     } 
  
}
  
