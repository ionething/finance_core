/*
 *  @Title UUIDUtil.java
 *  @Package： com.phoenix.core.util
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.util;

import java.util.UUID;

/**
 *  @ClassName UUIDUtil
 *  @Description 生成随机主键
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年4月21日
 */
public final class UUIDUtil {

	/**
	 * 生成随机UUID
	 * 
	 * @return String
	 */
	public static String randomUUID() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

}
