/*
 *  @Title FileUtils.java
 *  @Package： com.phoenix.core.util
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.phoenix.core.logger.SimpleLogger;

/**
 *  @ClassName FileUtils
 *  @Description 文件工具类
 *  @author yijun
 *  @version 1.0
 *  @date 2017年7月7日
 */
public class FileUtils {

	private static SimpleLogger logger = SimpleLogger.getLogger(FileUtils.class);

	/**
	 * 以字节为单位读取文件，常用于读二进制文件，如图片、声音、影像等文件。
	 */
	public static void readFileByBytes(String fileName) {
		InputStream in = null;
		try {
			// 一次读多个字节
			byte[] tempbytes = new byte[100];
			int byteread = 0;
			in = new FileInputStream(fileName);
			// 读入多个字节到字节数组中，byteread为一次读入的字节数
			while ((byteread = in.read(tempbytes)) != -1) {
				System.out.write(tempbytes, 0, byteread);
			}
		} catch (Exception e) {
			logger.error(" readFileByBytes Exception", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
					logger.error(" readFileByBytes IOException", e1);
				}
			}
		}
	}

	/**
	 * 以行为单位读取文件，常用于读面向行的格式化文件
	 */
	public static List<String> readFileByLines(String fileName, String charSet) {
		BufferedReader reader = null;
		List<String> list = new ArrayList<String>();
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), charSet));
			String tempString = null;
			// 一次读入一行，直到读入null为文件结束
			while ((tempString = reader.readLine()) != null) {
				list.add(tempString);
			}
			reader.close();
		} catch (IOException e) {
			logger.error(" readFileByLines Exception", e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
					logger.error(" readFileByLines IOException", e1);
				}
			}
		}
		return list;
	}

	private static String imgRootPath = "";
	private static String docRootPath = "";

	/**
	 * 根据mongoId 拼接文件地址
	 * 
	 * @param mongoId
	 * @return String
	 */
	public static String getFileUrl(String mongoId) {
		if (StringUtils.isBlank(imgRootPath)) {
			imgRootPath = GeneralProperties.getProperty("picture.root");
			if (imgRootPath.lastIndexOf("/") != imgRootPath.length() - 1) // 判断参数末尾是否有斜杠
				imgRootPath += "/";
		}
		return imgRootPath + mongoId;
	}

	/**
	 * 根据mongoId 拼接文件地址
	 * 
	 * @param mongoId
	 * @return String
	 */
	public static String getFileUrl(String mongoId, String fileSuffix) {
		String reg = "^(.JPEG|.jpeg|.JPG|.jpg|.GIF|.gif|.BMP|.bmp|.PNG|.png)$";
		if (!Pattern.compile(reg).matcher(fileSuffix.toLowerCase()).find()) {
			docRootPath = GeneralProperties.getProperty("document.root");
			if (docRootPath.lastIndexOf("/") != docRootPath.length() - 1) // 判断参数末尾是否有斜杠
				docRootPath += "/";
			return docRootPath + mongoId;
		} else {
			imgRootPath = GeneralProperties.getProperty("picture.root");
			if (imgRootPath.lastIndexOf("/") != imgRootPath.length() - 1) // 判断参数末尾是否有斜杠
				imgRootPath += "/";
			return imgRootPath + mongoId;
		}

	}

	/**
	 * 写文件
	 */
	public static boolean wirte(String filePath, String fileContent, boolean append) {
		File n = new File(filePath);
		FileWriter fw = null;
		try {
			fw = new FileWriter(n, append);
			fw.write(fileContent);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (fw != null)
					fw.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
