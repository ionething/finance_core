/*
 *  @Title AnnotationUtil.java
 *  @Package： com.phoenix.core.util
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.util;

import java.lang.annotation.Annotation;

import org.springframework.stereotype.Service;

/**
 *  @ClassName AnnotationUtil
 *  @Description 类注释
 *  @author liuwb
 *  @version 1.0
 *  @date 2017年7月20日
 */
public class AnnotationUtil {

	/** 获取指定类所有注解 */
	public static <T> Annotation[] getAnnotations(Class<T> srcClass) {
		return srcClass == null ? null : srcClass.getAnnotations();
	}

	/** 获取指定注解 */
	public static <T, A extends Annotation> A getAnnotation(Class<T> srcClass, Class<A> annotationClass) {
		return srcClass == null ? null : srcClass.getAnnotation(annotationClass);
	}

	/** 获取spring.service注解 */
	public static <T> Service getSpringServiceAnnotation(Class<T> srcClass) {
		return srcClass == null ? null : srcClass.getAnnotation(Service.class);
	}

	/** 获取spring.service注解的value属性值 */
	public static <T> String getSpringServiceAnnotationValue(Class<T> srcClass) {
		Service service = getSpringServiceAnnotation(srcClass);
		return service == null ? null : service.value();
	}
}
