/*
 *  @Title BigDecimalUtils.java
 *  @Package： com.phoenix.core.util
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;

import com.phoenix.core.enums.CalculateType;

/**
 *  @ClassName BigDecimalUtils
 *  @Description 工具类
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年7月15日
 */
public final class BigDecimalUtils {

	/** 10000 */
	public static final BigDecimal TEN_THOUSAND = new BigDecimal("10000");
	/** 100 */
	public static final BigDecimal ONE_HUNDRED = new BigDecimal("100");
	/** 365 */
	public static final BigDecimal DAYS_OF_YARD = new BigDecimal("365");
	/** 360 */
	public static final BigDecimal VALUE_360 = new BigDecimal("360");
	public static final BigDecimal VALUE_500 = new BigDecimal("500");
	/** 12 */
	public static final BigDecimal TWELVE = new BigDecimal("12");
	/** 1000 */
	public static final BigDecimal ONE_THOUSAND = new BigDecimal("1000");
	/** 10 */
	public static final BigDecimal TEN = BigDecimal.TEN;
	/** 3 */
	public static final BigDecimal THREE = new BigDecimal("3");
	/** 2 */
	public static final BigDecimal TWO = new BigDecimal("2");
	/** 0.5,二分之一 */
	public static final BigDecimal HAFL = new BigDecimal("0.5");
	/** 0.75,四分之三 */
	public static final BigDecimal THREE_QUARTERS = new BigDecimal("0.75");
	/** 0.55 */
	public static final BigDecimal FIFTY_FIVE_PERCENT = new BigDecimal("0.55");
	/** 0.1 */
	public static final BigDecimal TENTH = new BigDecimal("0.1");
	/** 0.01 */
	public static final BigDecimal HUNDREDTH = new BigDecimal("0.01");

	public static final DecimalFormat INTEGER = new DecimalFormat("0");

	public static final DecimalFormat TWO_DECIMAL = new DecimalFormat("0.00");

	public static final DecimalFormat AUTO_TWO = new DecimalFormat("#.##");

	public static final DecimalFormat FOUR_DECIMAL = new DecimalFormat("0.0000");

	public static final DecimalFormat AUTO_FOUR = new DecimalFormat("#.####");

	public static final DecimalFormat BILLOIN = new DecimalFormat("0000000000");

	public static final DecimalFormat EIGHT_DECIMAL = new DecimalFormat("0.00000000");

	public static final DecimalFormat AUTO_EIGHT = new DecimalFormat("#.########");

	public static final DecimalFormat AUTO_COMMA = new DecimalFormat(",##0.00");// 123,123.00

	public static final DecimalFormat AUTO_COMMA_POINT = new DecimalFormat(",##0.##");// 123,123.00

	// 精确到2位 (对外返回)
	public static final int PRECISION_ROUND_SCALE = 2;
	// 精确到8位(对内计算)
	public static final int PRECISION_SCALE = 8;

	/**
	 * 返回保留两位精确的数字<br>
	 * 第三位 <=5 舍<br>
	 * >5入<br>
	 * 
	 * @param rtnAmt
	 * @return BigDecimal
	 */
	public static BigDecimal getReturnRound(BigDecimal rtnAmt) {
		if (rtnAmt == null)
			return BigDecimal.ZERO;
		rtnAmt = rtnAmt.setScale(PRECISION_ROUND_SCALE, BigDecimal.ROUND_HALF_DOWN);
		return rtnAmt;
	}

	/**
	 * 格式化 BigDecimal为 xxx.xx格式字符串
	 * 
	 * @param amt
	 * @return
	 */
	public static String format(BigDecimal amt) {
		DecimalFormat df = new DecimalFormat("#0.00");
		return df.format(amt);
	}

	/**
	 * 四舍五入保留两位精度
	 * 
	 * @param rtnAmt
	 * @return BigDecimal
	 */
	public static BigDecimal getReturnRoundUp(BigDecimal rtnAmt) {
		if (rtnAmt == null)
			return BigDecimal.ZERO;
		rtnAmt = rtnAmt.setScale(PRECISION_ROUND_SCALE, BigDecimal.ROUND_HALF_UP);
		return rtnAmt;
	}

	/**
	 * 舍掉保留两位小数
	 * 
	 * @param value
	 * @return BigDecimal
	 */
	public static BigDecimal getReturnRoundDown(BigDecimal value) {
		if (value == null)
			return BigDecimal.ZERO;
		return value.setScale(PRECISION_ROUND_SCALE, BigDecimal.ROUND_DOWN);
	}

	/**
	 * 设置精度 为2位,并截取小数点后两位 <br>
	 * 正数 舍 负数 入
	 * 
	 * @return
	 */
	public static BigDecimal setPrecisionTwoFloor(BigDecimal value) {
		if (value == null)
			return BigDecimal.ZERO;
		return value.setScale(PRECISION_ROUND_SCALE, BigDecimal.ROUND_FLOOR);
	}

	/**
	 * 根据返回策略返回两位精度
	 * 
	 * @param rtnAmt
	 * @param RoundingMode
	 * @return BigDecimal
	 */
	public static BigDecimal getReturnRound(BigDecimal rtnAmt, int RoundingMode) {
		if (rtnAmt == null)
			return BigDecimal.ZERO;
		rtnAmt = rtnAmt.setScale(PRECISION_ROUND_SCALE, RoundingMode);
		return rtnAmt;
	}

	/**
	 * BigDecimal处理
	 * 
	 * @param :type
	 *            处理加减乘除(multiply乘法, add加法，divide 除 , subtract减法 )
	 * 
	 */
	public static BigDecimal getValue(BigDecimal number1, BigDecimal number2, CalculateType calculateType) {
		BigDecimal result = null;
		switch (calculateType) {
		case Add:
			result = number1.add(number2);
			break;
		case Subtract:
			result = number1.subtract(number2);
			break;
		case Multiply:
			result = number1.multiply(number2);
			break;
		case Divide:
			result = number1.divide(number2, PRECISION_SCALE, BigDecimal.ROUND_HALF_DOWN);
			break;
		default:
			break;
		}
		return result;

	}

	/**
	 * BigDecimal处理
	 * 
	 * @param :百分比的转换
	 * @param number1
	 *            要转换成的百分数 (针对业务定制的，百分比的换算)
	 * 
	 */
	public static BigDecimal getPerCent(BigDecimal number1) {
		BigDecimal result = number1.divide(ONE_HUNDRED);
		result = getValue(result, new BigDecimal(1), CalculateType.Add);
		return result;
	}

	/**
	 * BigDecimal处理
	 * 
	 * @param :总金额计算
	 * @param number1
	 *            表示借款的金额,number2表示要乘以的百分比（例如1.05）
	 * @author 谢新诚
	 * 
	 */
	public static BigDecimal getPerMoney(BigDecimal number1, BigDecimal number2) {
		BigDecimal result = getValue(number1, getPerCent(number2), CalculateType.Multiply);
		return result;
	}

	/**
	 * 格式化 BigDecimal为xxx,xxx.xx格式字符串，如123123.123格式化为123,123.12
	 * 
	 * @param number
	 * @return
	 */
	public static String formatComma(BigDecimal number) {
		return AUTO_COMMA.format(number);
	}

	/**
	 * 格式化 BigDecimal为xxx,xxx或xxx,xxx.xx格式字符串
	 * 
	 * @param number
	 * @return
	 */
	public static String formatCommaPoint(BigDecimal number) {
		return AUTO_COMMA_POINT.format(number);
	}

	/**
	 * BigDecimal处理,舍去小数,获取整数
	 */
	public static BigDecimal getIntegerDown(BigDecimal value) {
		if (value == null)
			return BigDecimal.ZERO;
		return value.setScale(0, BigDecimal.ROUND_DOWN);
	}

	/**
	 * 判断BigDecimal值是否大于0
	 * 
	 * @param value
	 * @return
	 */
	public static Boolean isGreaterZero(BigDecimal value) {
		if (value == null) {
			value = new BigDecimal(BigInteger.ZERO);
		}
		return value.compareTo(BigDecimal.ZERO) > 0;
	}
	/**
	 * 判断BigDecimal值是否大于0
	 * 
	 * @param value
	 * @return
	 */
	public static Boolean isZero(BigDecimal value) {
		if (value == null) {
			value = new BigDecimal(BigInteger.ZERO);
		}
		return value.compareTo(BigDecimal.ZERO) == 0;
	}

	/**
	 * BigDecimal值比较
	 * 
	 * @param soureValue
	 * @param targetValue
	 * @return
	 */
	public static Boolean isGreaterValue(BigDecimal soureValue, BigDecimal targetValue) {
		if (soureValue == null) {
			soureValue = new BigDecimal(BigInteger.ZERO);
		}
		if (targetValue == null) {
			targetValue = new BigDecimal(BigInteger.ZERO);
		}
		return soureValue.compareTo(targetValue) > 0;

	}

	/**
	 * 判断一个数soureValue是否处于minValue和maxValue之间
	 * 
	 * @param soureValue
	 * @param minValue
	 *            null表示无穷小
	 * @param maxValue
	 *            null表示无穷大
	 * @return
	 */
	public static boolean isBetween(BigDecimal soureValue, BigDecimal minValue, BigDecimal maxValue) {
		if (soureValue == null) {
			soureValue = BigDecimal.ZERO;
		}
		if (minValue == null && maxValue == null) {
			return Boolean.TRUE;
		}
		if (maxValue == null) {
			return soureValue.compareTo(minValue) >= 0;
		}
		if (minValue == null) {
			return soureValue.compareTo(maxValue) <= 0;
		}
		return soureValue.compareTo(minValue) >= 0 && soureValue.compareTo(maxValue) <= 0;

	}

	/**
	 * 返回一个不为null的数值，如果等于为空 返回0
	 * 
	 * @param soureValue
	 * @return
	 */
	public static BigDecimal getNotNullValue(BigDecimal soureValue) {
		return soureValue == null ? BigDecimal.ZERO : soureValue;
	}

	/** 
	 * @Title: getFormatNoZero 
	 * @Description: 格式化数字
	 * @param val
	 * @return
	 * @return: String
	 */
	public static String getFormatNoZero(BigDecimal val) {
		val = getNotNullValue(val);
		DecimalFormat df = new DecimalFormat("##,##0.##");
		String price = df.format(val);
		return price;
	}
}
