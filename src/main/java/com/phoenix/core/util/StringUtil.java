/*
 *  @Title StringUtil.java
 *  @Package： com.phoenix.core.util
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.util;

import java.util.Random;

import org.apache.commons.lang3.StringUtils;

/**
 *  @ClassName StringUtil
 *  @Description 类注释
 *  @author liuwb
 *  @version 1.0
 *  @date 2017年6月24日
 */
public class StringUtil {

	public static boolean isEmpty(String str) {
		return StringUtils.isEmpty(str);
	}

	/**
	 * java生成随机数字和字母组合
	 * 
	 * @param length
	 *            [生成随机数的长度]
	 * @return
	 */
	public static String getCharAndNumr(int length) {
		String val = "";
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			// 输出字母还是数字
			String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
			// 字符串
			if ("char".equalsIgnoreCase(charOrNum)) {
				// 取得大写字母还是小写字母
				int choice = random.nextInt(2) % 2 == 0 ? 65 : 97;
				val += (char) (choice + random.nextInt(26));
			} else if ("num".equalsIgnoreCase(charOrNum)) { // 数字
				val += String.valueOf(random.nextInt(10));
			}
		}
		return val;
	}

	/**
	 * 去除富文本中的html标签
	 * 
	 * @param text
	 * @return
	 */
	public static String deleteHtmlTag(String text) {
		if (!isEmpty(text)) {
			return text.replaceAll("<[a-zA-Z]+[1-9]?[^><]*>", "").replaceAll("</[a-zA-Z]+[1-9]?>", "");
		}
		return "";
	}

	public static String subStringLeft(String source, int length) {
		if (isEmpty(source))
			return "";

		if (length <= 0)
			return "";

		if (length >= source.length())
			return source;

		return source.substring(0, length);
	}

	public static String subStringLeftRight(String source, int length) {
		if (isEmpty(source))
			return "";

		if (length <= 0)
			return "";

		if (length >= source.length())
			return source;

		int end = source.length();
		int start = end - length;

		return source.substring(start, end);
	}

	/**
	 * 首字母大写
	 * 
	 * @param val
	 * @return
	 */
	public static String firstUpper(String val) {
		if (StringUtils.isBlank(val))
			return "";

		val = val.replaceAll("_", "");
		String first = val.substring(0, 1).toUpperCase();
		String other = val.substring(1, val.length());
		return new StringBuffer(first).append(other).toString();
	}

	/**
	 * 替换所有空格
	 */
	public static String trimSpace(String s) {
		if (s == null)
			return "";
		return s.replace(" ", "");
	}

	/**
	 * 替换所有空白符, 包括空格、制表符、换页符等空白字符
	 */
	public static String trimBlank(String s) {
		if (s == null)
			return "";
		return s.replaceAll("\\s*", "");
	}
}
