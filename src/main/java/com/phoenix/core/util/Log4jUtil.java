/*
 *  @Title Log4jUtil.java
 *  @Package： com.phoenix.core.util
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.util;

import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Priority;

/**
 *  @ClassName Log4jUtil
 *  @Description 实现不同级别日志输出到不同的日志文件
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年6月8日
 */
public class Log4jUtil extends DailyRollingFileAppender {
	@Override
	public boolean isAsSevereAsThreshold(Priority priority) {
		// 只判断是否相等，而不判断优先级
		return this.getThreshold().equals(priority);
	}
}
