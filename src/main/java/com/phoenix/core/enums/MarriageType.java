/*
 *  @Title MarriageType.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

import java.util.Arrays;
import java.util.List;

/**
 *  @ClassName MarriageType
 *  @Description 婚姻状态枚举类
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年5月8日
 */
public enum MarriageType {

	YIH("1", "已婚"),
	WEIH("2", "未婚"),
	LIY("3", "离异"),
	SANGO("4", "丧偶"),
	QITA("5", "其他");

	private String code;
	private String cnName;

	MarriageType(String code, String cnName) {
		this.code = code;
		this.cnName = cnName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCnName() {
		return cnName;
	}

	public void setCnName(String cnName) {
		this.cnName = cnName;
	}

	public static String getCnNameByCode(String code) {
		for (MarriageType us : MarriageType.values()) {
			if (us.getCode().equals(code)) {
				return us.getCnName();
			}
		}
		return "";
	}

	public static String getCodeByCnName(String cnName) {
		for (MarriageType yesOrNo : MarriageType.values()) {
			if (yesOrNo.getCnName().equals(cnName)) {
				return yesOrNo.getCode();
			}
		}
		return "";
	}
	public static List<MarriageType> getAllList(){
		return Arrays.asList(MarriageType.values());
	}
}
