/*
 *  @Title IsValid.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

/**
 *  @ClassName IsValid
 *  @Description 是否有效枚举
 *  @author liuwei
 *  @version 1.0
 *  @date 2017年7月3日
 */
public enum IsValid {
	
	INVALID("0", "无效"), VALID("1", "有效"), UNVALID("2", "未生效");

	private String value;
	private String name;

	IsValid(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static String getNameByValue(String value) {
		for (IsValid att : IsValid.values()) {
			if (att.getValue().equals(value))
				return att.getName();
		}
		return null;
	}
}
