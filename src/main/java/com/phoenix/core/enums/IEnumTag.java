/*
 *  @Title IEnumTag.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

/**
 *  @ClassName IEnumTag
 *  @Description 枚举类型标签接口
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年6月28日
 */
public interface IEnumTag {

	/**
	 * 根据枚举类型的value属性取对应的name属性值
	 * 
	 * @return name
	 */
	public String getName(String value);

}
