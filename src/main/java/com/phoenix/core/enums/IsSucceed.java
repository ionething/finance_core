/*
 *  @Title IsSucceed.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

/**
 *  @ClassName IsSucceed
 *  @Description 是否成功枚举
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年4月22日
 */
public enum IsSucceed {

	SUCCEED("0", "成功"), FAILURE("1", "失败");

	private String value;
	private String name;

	IsSucceed(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static String getNameByValue(String value) {
		for (IsSucceed att : IsSucceed.values()) {
			if (att.getValue().equals(value))
				return att.getName();
		}
		return null;
	}
}
