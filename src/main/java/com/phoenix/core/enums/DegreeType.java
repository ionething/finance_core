/*
 *  @Title DegreeType.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *  @ClassName DegreeType
 *  @Description 学历类型枚举类
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年6月21日
 */
public enum DegreeType {
	
	XIAOXUE_SCHOOL("0", "小学"),
	JUNIOR_SCHOOL("1", "初中"),
	SENIOR_SCHOOL("2", "高中"),
	POLYTECHNIC_SCHOOL("3", "中专"),
	COLLEGE_SCHOOL("4", "大专"),
	UNDERGRADUATE("5", "本科"),
	MASTER("6", "硕士"),
	DOCTOR("7", "博士"),
	POSTDOCTORAL("8", "博士后"),
	OTHER("9","其它");

	private final String value;
	private final String cnName;

	DegreeType(String value, String cnName) {
		this.value = value;
		this.cnName = cnName;
	}

	public String getValue() {
		return value;
	}

	public String getCnName() {
		return cnName;
	}
	public static Map<String, Object> getMap(){
		Map<String,Object> map = new LinkedHashMap<String, Object>();
		for(DegreeType degreeType : DegreeType.values())
			map.put(degreeType.getValue(), degreeType.getCnName());
		return map;
		
	}
	
	public static String getNameForValue(String val) {
		for(DegreeType att: DegreeType.values()){
			if(att.getValue().equals(val))
				return att.getCnName();
		}
		return null;
	}
	public static List<DegreeType> getAllList(){
		return Arrays.asList(DegreeType.values());
	}
 }
