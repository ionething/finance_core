/*
 *  @Title ResultType.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

/**
 *  @ClassName ResultType
 *  @Description 返回结果类型枚举类
 *  @author liuwei
 *  @version 1.0
 *  @date 2017年7月6日
 */
public enum ResultType {

	RESULT_SUCCESS("0", "成功"), RESULT_FAIL("1", "失败"), RESULT_EXCEPTION("2", "异常");

	private final String value;
	private final String name;

	ResultType(String value, String name) {
		this.name = name;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public static String getNameByValue(String value) {
		for (ResultType rt : ResultType.values()) {
			if (rt.getValue().equals(value))
				return rt.getName();
		}
		return null;
	}
}
