/*
 *  @Title UserSex.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

import java.util.Arrays;
import java.util.List;

/**
 *  @ClassName UserSex
 *  @Description 用户性别枚举类
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年5月14日
 */
public enum UserSex {

	MAN("M", "男"), WOMAN("F", "女");

	private String code;
	private String cnName;

	UserSex(String code, String cnName) {
		this.code = code;
		this.cnName = cnName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCnName() {
		return cnName;
	}

	public void setCnName(String cnName) {
		this.cnName = cnName;
	}

	public static String getCnNameByCode(String code) {
		for (UserSex us : UserSex.values()) {
			if (us.getCode().equals(code)) {
				return us.getCnName();
			}
		}
		return "";
	}

	public static String getCodeByCnName(String cnName) {
		for (UserSex yesOrNo : UserSex.values()) {
			if (yesOrNo.getCnName().equals(cnName)) {
				return yesOrNo.getCode();
			}
		}
		return "";
	}

	public static List<UserSex> getAllList() {
		return Arrays.asList(UserSex.values());
	}
}
