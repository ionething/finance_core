/*
 *  @Title HouseType.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

import java.util.Arrays;
import java.util.List;

/**
 *  @ClassName HouseType
 *  @Description 公司性质枚举类
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年6月14日
 */
public enum HouseType {

	ZIYZF("1", "自有住房"),
	ZAIGZF("2", "在供住房"),
	ZUF("3", "租房"),
	JITSS("4", "集体宿舍"),
	QIT("5", "其他");

	private String code;
	private String cnName;

	HouseType(String code, String cnName) {
		this.code = code;
		this.cnName = cnName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCnName() {
		return cnName;
	}

	public void setCnName(String cnName) {
		this.cnName = cnName;
	}

	public static String getCnNameByCode(String code) {
		for (HouseType us : HouseType.values()) {
			if (us.getCode().equals(code)) {
				return us.getCnName();
			}
		}
		return "";
	}

	public static String getCodeByCnName(String cnName) {
		for (HouseType yesOrNo : HouseType.values()) {
			if (yesOrNo.getCnName().equals(cnName)) {
				return yesOrNo.getCode();
			}
		}
		return "";
	}
	public static List<HouseType> getAllList(){
		return Arrays.asList(HouseType.values());
	}
}
