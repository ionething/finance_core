/*
 *  @Title CompanyNature.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

import java.util.Arrays;
import java.util.List;

/**
 *  @ClassName CompanyNature
 *  @Description 公司性质枚举类
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年7月9日
 */
public enum CompanyNature {

	GUOYGF("1", "国有股份"),
	WAIZQY("2", "外资企业"),
	SHEHTT("3", "社会团体"),
	HEZQY("4", "合资企业"),
	MINYQY("5", "民营企业"),
	JIGSY("6", "机关事业"),
	GET("7", "个体"),
	SIYQY("8", "私营企业"),
	QIT("9", "其他");

	private String code;
	private String cnName;

	CompanyNature(String code, String cnName) {
		this.code = code;
		this.cnName = cnName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCnName() {
		return cnName;
	}

	public void setCnName(String cnName) {
		this.cnName = cnName;
	}

	public static String getCnNameByCode(String code) {
		for (CompanyNature us : CompanyNature.values()) {
			if (us.getCode().equals(code)) {
				return us.getCnName();
			}
		}
		return "";
	}

	public static String getCodeByCnName(String cnName) {
		for (CompanyNature yesOrNo : CompanyNature.values()) {
			if (yesOrNo.getCnName().equals(cnName)) {
				return yesOrNo.getCode();
			}
		}
		return "";
	}
	public static List<CompanyNature> getAllList(){
		return Arrays.asList(CompanyNature.values());
	}
}
