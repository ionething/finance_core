/*
 *  @Title IdNoType.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

import java.util.Arrays;
import java.util.List;

/**
 *  @ClassName IdNoType
 *  @Description 证件类型枚举类
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年7月29日
 */
public enum IdNoType {

	SHENFZ("0", "身份证"),
	HUZ("1", "护照"),
	YINGYZZ("2", "营业执照");

	private String code;
	private String cnName;

	IdNoType(String code, String cnName) {
		this.code = code;
		this.cnName = cnName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCnName() {
		return cnName;
	}

	public void setCnName(String cnName) {
		this.cnName = cnName;
	}

	public static String getCnNameByCode(String code) {
		for (IdNoType us : IdNoType.values()) {
			if (us.getCode().equals(code)) {
				return us.getCnName();
			}
		}
		return "";
	}

	public static String getCodeByCnName(String cnName) {
		for (IdNoType yesOrNo : IdNoType.values()) {
			if (yesOrNo.getCnName().equals(cnName)) {
				return yesOrNo.getCode();
			}
		}
		return "";
	}
	public static List<IdNoType> getAllList(){
		return Arrays.asList(IdNoType.values());
	}
}
