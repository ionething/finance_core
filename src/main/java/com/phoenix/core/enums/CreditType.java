/*
 *  @Title CreditType.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

import java.util.Arrays;
import java.util.List;

/**
 *  @ClassName CreditType
 *  @Description 信用情况枚举类
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年7月15日
 */
public enum CreditType {

	NO_CREDIT("0", "无信用记录"),
	GOOT_CREDIT("1", "信用良好"),
	FEW_OVERDUE("2", "少数逾期"),
	MOST_OVERDUE("3", "长期逾期");

	private String code;
	private String cnName;

	CreditType(String code, String cnName) {
		this.code = code;
		this.cnName = cnName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCnName() {
		return cnName;
	}

	public void setCnName(String cnName) {
		this.cnName = cnName;
	}

	public static String getCnNameByCode(String code) {
		for (CreditType us : CreditType.values()) {
			if (us.getCode().equals(code)) {
				return us.getCnName();
			}
		}
		return "";
	}

	public static String getCodeByCnName(String cnName) {
		for (CreditType yesOrNo : CreditType.values()) {
			if (yesOrNo.getCnName().equals(cnName)) {
				return yesOrNo.getCode();
			}
		}
		return "";
	}
	public static List<CreditType> getAllList(){
		return Arrays.asList(CreditType.values());
	}
}
