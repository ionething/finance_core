/*
 *  @Title PaperworkType.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

/**
 *  @ClassName PaperworkType
 *  @Description 传文件资料类型
 *  @author liuwb
 *  @version 1.0
 *  @date 2017年6月27日
 */
public enum PaperworkType {

	/** 身份证 */
	ID_CARD("0", "身份证"),
	/** 身份证反面 */
	ID_CARD_BACK("1", "身份证反面"),
	/** 用户头像 */
	USER_AVATAR("2", "用户头像"),
	/** 通过id5验证返回的真实身份证上的照片 */
	PERSON_INFO("3", "个人照片"),
	/** 信用报告 */
	CREDIT_REPORT("4", "信用报告"),
	/** 收入证明 */
	INCOME_PROVE("5", "收入证明"),
	/** 房子证明 */
	HOUSE_PROVE("6", "房子证明"),
	/** 住址证明 */
	HOUSE_ADDRESS("7", "住址证明"),
	/** 学校证明 */
	SCHOOL_PROVE("8", "学校证明"),
	/** 户口复印件 */
	HOUSEHOLD("9", "户口本复印件"),
	/** 工作证明 */
	WORK_PROVE("10", "工作证明"),
	/** 公司证明 */
	COMPANY_PROVE("11", "公司证明"),
	/** 公司房产证明 */
	COMPANY_HOUSE_PROVE("12", "公司房产证明");

	private final String value;
	private final String cnName;

	PaperworkType(String value, String cnName) {
		this.value = value;
		this.cnName = cnName;
	}

	public String getValue() {
		return value;
	}

	public String getCnName() {
		return cnName;
	}

}
