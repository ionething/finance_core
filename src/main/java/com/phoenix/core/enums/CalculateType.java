/*
 *  @Title CalculateType.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

/**
 *  @ClassName CalculateType
 *  @Description 计算类型
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年7月19日
 */
public enum CalculateType {

	Add("add", "加法"), Subtract("subtract", "减法"), Multiply("multiply", "乘法"), Divide("divide", "除法");

	private String value;

	private String name;

	CalculateType(String value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
