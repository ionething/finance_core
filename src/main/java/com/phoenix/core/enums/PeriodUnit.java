/*
 *  @Title PeriodUnit.java
 *  @Package： com.phoenix.core.enums
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.enums;

import java.util.Arrays;
import java.util.List;

/**
 *  @ClassName PeriodUnit
 *  @Description 周期枚举类
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年7月12日
 */
public enum PeriodUnit {

	DAY_CA("0", "天"), MONTH_CA("1", "月"), YEAR_CA("2", "年");

	private final String value;
	private final String cnName;

	private PeriodUnit(String value, String cnName) {
		this.cnName = cnName;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getCnName() {
		return cnName;
	}

	public static String getCnNameByValue(String value) {
		for (PeriodUnit as : PeriodUnit.values())
			if (as.getValue().equals(value))
				return as.getCnName();
		return "";
	}

	public static PeriodUnit parse(String value) {
		for (PeriodUnit as : PeriodUnit.values())
			if (as.getValue().equals(value))
				return as;
		return null;
	}

	public static List<PeriodUnit> getAllList() {
		return Arrays.asList(PeriodUnit.values());

	}
}
