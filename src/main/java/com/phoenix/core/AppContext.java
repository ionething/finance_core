/*
 *  @Title AppContext.java
 *  @Package： com.phoenix.core
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core;


import com.phoenix.core.logger.SimpleLogger;

/**
 *  @ClassName AppContext
 *  @Description 系统上下文信息公共属性
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年5月20日
 */
public class AppContext {

	/** 上下文路径 */
	public static String realPath = "";

	static SimpleLogger logger = SimpleLogger.getLogger(AppContext.class);

	/** 设置系统上下文路径 */
	public static void setRealPath(String path) {
		if ("".equals(realPath))
			realPath = path;
	}

	public static String getRealPath() {
		return realPath;
	}

}
