/*
 *  @Title SimpleLoggerInitServlet.java
 *  @Package： com.phoenix.core.logger
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.PropertyConfigurator;
import org.springframework.core.io.ClassPathResource;

/**
 * @ClassName SimpleLoggerInitServlet
 * @Description 日志初始化Servlet<br>
 *              若使用disconf托管log4j.properties则在web.xml添加以下代码:
 * 
 *              &lt;servlet&gt; <servlet-name>log4j-init</servlet-name>
 *              <servlet-class>com.phoenix.core.logger.SimpleLoggerInitServlet</servlet-class>
 *              <load-on-startup>1</load-on-startup> &lt;servlet&gt;
 * 
 * @author yij
 * @version 1.0
 * @date 2017年4月27日
 */
public class SimpleLoggerInitServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5546903019420102335L;

	private static final String FILE_NAME = "log4j.properties";

	/**
	 * 现在使用disconf托管log4j.properties,disconf由spring加载,<br>
	 * 故而初始化log4j配置,应该在spring ContextLoaderListener之后<br>
	 * web.xml的加载顺序一般是这样的 context-param,listener,filter, servlet<br>
	 */
	@Override
	public void init() throws ServletException {
		super.init();
		ClassPathResource rs = new ClassPathResource(FILE_NAME);
		if (!rs.exists()) {
			throw new IllegalArgumentException("日志配置文件[log4j.properties]不存在class目录中");
		}
		String path = this.getClass().getClassLoader().getResource(FILE_NAME).getFile();
		PropertyConfigurator.configure(path);
	}
}
