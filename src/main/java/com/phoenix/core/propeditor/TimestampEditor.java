/*
 *  @Title TimestampEditor.java
 *  @Package： com.phoenix.core.propeditor
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.propeditor;

import java.beans.PropertyEditorSupport;
import java.sql.Timestamp;

import org.apache.commons.lang.StringUtils;

/**
 *  @ClassName TimestampEditor
 *  @Description 时间格式化
 *  @author liuwenbin
 *  @version 1.0
 *  @date 2017年4月23日
 */
public class TimestampEditor extends PropertyEditorSupport {

	public void setAsText(String text) {
		if (StringUtils.isNotBlank(text)) {
			setValue(Timestamp.valueOf(text));
		}
	}

}
