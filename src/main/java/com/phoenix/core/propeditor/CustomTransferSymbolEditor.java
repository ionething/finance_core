/*
 *  @Title CustomTransferSymbolEditor.java
 *  @Package： com.phoenix.core.propeditor
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.propeditor;

import java.beans.PropertyEditorSupport;

import com.phoenix.core.util.BaseUtils;

/**
 *  @ClassName CustomTransferSymbolEditor
 *  @Description 对输入字符进行转义处理
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年5月7日
 */
public class CustomTransferSymbolEditor extends PropertyEditorSupport {

	public void setAsText(String text) {
		setValue(BaseUtils.replaceHtmlSymbols(text)); // 去掉字符串中的'<' '>' '"'字符
	}

}
