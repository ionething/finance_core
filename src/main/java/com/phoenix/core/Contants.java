/*
 *  @Title Contants.java
 *  @Package： com.phoenix.core
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core;

import java.math.BigDecimal;

/**
 *  @ClassName Contants
 *  @Description 公共常量
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年5月13日
 */
public final class Contants {

	// 系统公共
	// 人民银行基准利率（一年期） 2012发布
	public static final BigDecimal basicRate = new BigDecimal("0.03");
	public static String LetterSender = "";
	// web登录相关
	public final static String MEMBER_USER = "_p2p_member_user";// 登录会员对象
	public final static String VALID_TOKEN = "_p2p_session_token_";// 登录会员对象
	public final static String SECURITY_ISLOGIN = "_p2p_is_login_";// 用户登录
	public final static String SECURITY_NOLOGIN = "_p2p_nouser";// 用户是否存在
	public final static String SECURITY_LOGIN_LOCKED = "_p2p_user_locked";// 用户是否锁定
	public final static String SECURITY_VALIDCODE = "_p2p_code_error";// 验证码是否存在
	public final static String SECURITY_USERID = "_p2p_userId";// 用户ID
	public final static String SECURITY_USERACCOUNT = "_p2p_user_account";// 用户账号
	public final static String SECURITY_USEREMAIL = "_p2p_user_email";// 用户EMAIL
	public final static String SECURITY_USERMOBILE = "_p2p_user_mobile";// 用户手机
	public final static String SECURITY_USERNAME = "_p2p_user_name";// 用户姓名
	public final static String SECURITY_LOGIN_COUNT = "_p2p_user_login_count";// 登录次数
	
	public final static String SECURITY_LOCK_TIME = "login_fail_lock_time";// 登录锁定时间
	public final static String SECURITY_MAX_LOGIN_COUNT = "security_max_login_count";// 登录最大次数
}
