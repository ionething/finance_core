/*
 *  @Title RedirectResult.java
 *  @Package： com.phoenix.core.json
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.json;

import java.util.Map;

/**
 *  @ClassName RedirectResult
 *  @Description 类注释
 *  @author liuwenbin
 *  @version 1.0
 *  @date 2017年6月3日
 */
public class RedirectResult extends BusinessResult {

	private static final long serialVersionUID = -6782728551050265735L;
	private String postUrl;// 提交地址,适用于页面跳转
	private Map<String, String> postMap;// 提交表单,适用于页面跳转
	private String method = "post";
	private String target = "_self";

	public static RedirectResult getFailResult(String message) {
		RedirectResult result = new RedirectResult();
		result.setSuccessed(false);
		result.setMessage(message);
		return result;
	}

	public static RedirectResult getSuccessResult(String url, Map<String, String> param) {
		RedirectResult result = new RedirectResult();
		result.setSuccessed(true);
		result.setPostUrl(url);
		result.setPostMap(param);
		return result;
	}

	public String getPostUrl() {
		return postUrl;
	}

	public void setPostUrl(String postUrl) {
		this.postUrl = postUrl;
	}

	public Map<String, String> getPostMap() {
		return postMap;
	}

	public void setPostMap(Map<String, String> postMap) {
		this.postMap = postMap;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

}
