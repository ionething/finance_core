/*
 *  @Title BusinessResult.java
 *  @Package： com.phoenix.base.model
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.json;

import java.io.Serializable;

/**
 *  @ClassName BusinessResult
 *  @Description 业务返回结果
 *  @author liuwei
 *  @version 1.0
 *  @date 2017年6月15日
 */
public class BusinessResult implements Serializable {

	private static final long serialVersionUID = -1091361722188635714L;

	private boolean isSuccessed = Boolean.FALSE;

	private String message;// 默认提示信息
	
	protected BusinessResult(){
		
	}

	public static BusinessResult getSuccessInstance() {
		BusinessResult result = new BusinessResult();
		result.setSuccessed(Boolean.TRUE);
		return result;
	}

	public static BusinessResult getSuccessInstance(String message) {
		BusinessResult result = new BusinessResult();
		result.setMessage(message);
		result.setSuccessed(Boolean.TRUE);
		return result;
	}

	public static BusinessResult getFailInstance(String message) {
		BusinessResult result = new BusinessResult();
		result.setMessage(message);
		result.setSuccessed(Boolean.FALSE);
		return result;
	}

	public static BusinessResult getFailInstance() {
		BusinessResult result = new BusinessResult();
		result.setSuccessed(Boolean.FALSE);
		return result;
	}

	public boolean isSuccessed() {
		return isSuccessed;
	}

	public void setSuccessed(boolean isSuccessed) {
		this.isSuccessed = isSuccessed;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
