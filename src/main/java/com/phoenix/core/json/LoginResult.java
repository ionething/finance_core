/*
 *  @Title LoginResult.java
 *  @Package： com.phoenix.core.json
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.json;

/**
 *  @ClassName LoginResult
 *  @Description 登录结果
 *  @author yijun
 *  @version 1.0
 *  @date 2017年7月27日
 */
public class LoginResult extends BusinessResult {

	private static final long serialVersionUID = 49655962884035822L;
	// 是否需要验证码
	private boolean captchaneed = Boolean.FALSE;

	public static LoginResult getFailResult() {
		return getFailResult(null, null);
	}

	public static LoginResult getFailResult(String message, Boolean captchaneed) {
		LoginResult result = new LoginResult();
		result.setSuccessed(false);
		result.setMessage(message);
		result.setCaptchaneed(captchaneed);
		return result;
	}

	public static LoginResult getFailResult(String message) {
		return getFailResult(message, null);
	}

	public static LoginResult getSuccessResult() {
		return getSuccessResult(null, null);
	}

	public static LoginResult getSuccessResult(String message, Boolean captchaneed) {
		LoginResult result = new LoginResult();
		result.setSuccessed(true);
		result.setMessage(message);
		return result;
	}

	public static LoginResult getSuccessResult(String message) {
		return getSuccessResult(message, null);
	}

	public boolean isCaptchaneed() {
		return captchaneed;
	}

	public void setCaptchaneed(boolean captchaneed) {
		this.captchaneed = captchaneed;
	}

}
