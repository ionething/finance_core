/*
 *  @Title JsonResult.java
 *  @Package： com.phoenix.core.json
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core.json;

import com.phoenix.core.ModelSerializable;

/**
 *  @ClassName JsonResult
 *  @Description 数据返回
 *  @author huangjx
 *  @version 1.0
 *  @date 2017年5月27日
 */
public class JsonResult implements ModelSerializable {

	private static final long serialVersionUID = -3941817907260975651L;
	private boolean success;
	private String code;
	private String msg;
	private String toUrl;
	private Object jsonData;

	public static JsonResult getFailResult(String msg) {
		JsonResult re = new JsonResult();
		re.setSuccess(false);
		re.setMsg(msg);
		return re;
	}
	public static JsonResult getSuccessResult(String msg) {
		JsonResult re = new JsonResult();
		re.setSuccess(true);
		re.setMsg(msg);
		return re;
	}

	public JsonResult() {
	}

	public JsonResult(boolean success, String msg, Object data) {
		this.success = success;
		this.msg = msg;
		this.jsonData = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getJsonData() {
		return jsonData;
	}

	public void setJsonData(Object jsonData) {
		this.jsonData = jsonData;
	}

	public String getToUrl() {
		return toUrl;
	}

	public void setToUrl(String toUrl) {
		this.toUrl = toUrl;
	}

}
