/*
 *  @Title ModelSerializable.java
 *  @Package： com.phoenix.core
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.core;

import java.io.Serializable;

/**
 *  @ClassName ModelSerializable
 *  @Description 对象序列化标示接口
 *  @author liuwb
 *  @version 1.0
 *  @date 2017年7月3日
 */
public interface ModelSerializable extends Serializable {

}
