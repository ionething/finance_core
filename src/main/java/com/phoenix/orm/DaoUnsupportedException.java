/*
 *  @Title DaoUnsupportedException.java
 *  @Package： com.phoenix.orm
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.orm;

import com.phoenix.exception.DataBaseAccessException;

/**
 *  @ClassName DaoUnsupportedException
 *  @Description 数据库异常操作
 *  @author liuwei
 *  @version 1.0
 *  @date 2017年6月14日
 */
public class DaoUnsupportedException extends DataBaseAccessException {

	private static final long serialVersionUID = 4928613476405775180L;

	public DaoUnsupportedException(String msg) {
		super(msg);
	}

	public DaoUnsupportedException() {
		super("this operation is not supported.");
	}

	public DaoUnsupportedException(Throwable cause) {
		super(cause);
	}
}
