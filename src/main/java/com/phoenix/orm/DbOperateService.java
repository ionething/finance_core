/*
 *  @Title DbOperateService.java
 *  @Package： com.phoenix.orm
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.orm;

import com.phoenix.exception.DataBaseAccessException;
import com.phoenix.orm.page.Page;

/**
 *  @ClassName DbOperateService
 *  @Description 数据库操作接口
 *  @author yijun
 *  @version 1.0
 *  @date 2017年6月21日
 */
public interface DbOperateService<T> {

	/**
	 * 带条件的分页查询
	 * 
	 * @param page
	 */
	public Page<T> getPage(T t, Page<T> page);

	/**
	 * 查询
	 * 
	 * @param key
	 * 
	 * @return String 提示信息
	 */
	public T findByKey(String key);

	/**
	 * 保存
	 * 
	 * @param user
	 *            操作的用户
	 * @param t
	 *            实例对象
	 * @return String 提示信息
	 * @throws DataBaseAccessException
	 */
	public boolean save(String user, T t) throws DataBaseAccessException;

	/**
	 * 修改
	 * 
	 * @param user
	 *            操作的用户
	 * @param t
	 *            实例对象
	 * @return String 提示信息
	 */
	public boolean update(String user, T t) throws DataBaseAccessException;

	/**
	 * 删除
	 * 
	 * @param user
	 *            操作的用户
	 * @param t
	 *            实例对象
	 * @return String 提示信息
	 */
	public boolean delete(String user, T t) throws DataBaseAccessException;

}
