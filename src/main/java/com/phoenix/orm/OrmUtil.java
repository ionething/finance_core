/*
 *  @Title OrmUtil.java
 *  @Package： com.phoenix.orm
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.orm;

import org.apache.commons.lang.StringUtils;

import com.phoenix.core.logger.SimpleLogger;
import com.phoenix.orm.page.Page;

/**
 *  @ClassName OrmUtil
 *  @Description 数据库底层工具类
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年6月7日
 */
public class OrmUtil {

	private static SimpleLogger logger = SimpleLogger.getLogger(OrmUtil.class);

	// 封装每页数据大小
	public static void formatPageSize(String pageNo, String limit, Page<?> page) {
		try {
			if (StringUtils.isNotBlank(pageNo)) {
				page.setPageNo(Integer.parseInt(pageNo));
			}
			if (StringUtils.isNotBlank(limit)) {
				page.setPageSize(Integer.parseInt(limit));
			}
		} catch (Exception e) {
			logger.error("page format excption for " + (page.getClass().getName()));
		}
	}
}
