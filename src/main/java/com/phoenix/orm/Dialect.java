/*
 *  @Title Dialect.java
 *  @Package： com.phoenix.orm
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.orm;

/**
 *  @ClassName Dialect
 *  @Description 方言接口各种数据库可实现自己的实现类
 *  @author liuwei
 *  @version 1.0
 *  @date 2017年6月28日
 */
public interface Dialect {

	boolean supportsLimit();

	boolean supportsLimitOffset();

	String getLimitString(String sql, int offset, int limit);

}
