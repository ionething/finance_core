/*
 *  @Title KeyGenerator.java
 *  @Package： com.phoenix.orm
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.orm;

import com.phoenix.core.util.UUIDUtil;

/**
 *  @ClassName KeyGenerator
 *  @Description 流水号产生器
 *  @author liuwb
 *  @version 1.0
 *  @date 2017年5月28日
 */
public class KeyGenerator {

	/**
	 * 获取随机UUID主键
	 * 
	 */
	public static String randomSeqNum() {
		return UUIDUtil.randomUUID();
	}

}
