/*
 *  @Title NestedException.java
 *  @Package： com.phoenix.base.exception
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.exception;

/**
 *  @ClassName NestedException
 *  @Description 自定义异常基类
 *  @author liuwei
 *  @version 1.0
 *  @date 2017年6月1日
 */
public class NestedException extends NestedRuntimeException {

	private static final long serialVersionUID = -4775190692869227607L;

	public NestedException(String msg) {
		super(msg);
	}

	public NestedException(Throwable cause) {
		super(cause);
	}

	public NestedException(String msg, Throwable cause) {
		super(msg, cause);
	}

	/**
	 * Return the detail message, including the message from the imtzp
	 * check exception if there is one.
	 */
	public String getMessage() {
		return super.getMessage(ExceptionDescriptor.Exception_DEF);
	}
}
