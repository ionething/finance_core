/*
 *  @Title ExceptionDescriptor.java
 *  @Package： com.phoenix.base.exception
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.exception;

/**
 *  @ClassName ExceptionDescriptor
 *  @Description 异常描叙
 *  @author wangyh
 *  @version 1.0
 *  @date 2017年7月23日
 */
public final class ExceptionDescriptor {

	// default Exception
	public static final int Exception_DEF = -1;
	// SQLException
	public static final int Exception_SQL = 0;

	// IndexOutOfBandsException
	public static final int Exception_IOB = 1;

	// ClassCastException
	public static final int Exception_CCE = 2;

	// NoClassDefFoundException
	public static final int Exception_NCF = 3;

	// SeccurityException
	public static final int Exception_SEC = 4;

	// NullPointerException
	public static final int Exception_NPE = 5;

	// mongodb Exception
	public static final int Exception_MOG = 6;

	// business Exception
	public static final int Exception_BIZ = 7;

}
