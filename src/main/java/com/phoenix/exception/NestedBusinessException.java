/*
 *  @Title NestedBusinessException.java
 *  @Package： com.phoenix.base.exception
 *  Copyright (c) 2017 by 江苏深南互联网金融信息服务有限公司  All right reserved
 */
package com.phoenix.exception;

/**
 *  @ClassName NestedBusinessException
 *  @Description 业务自定义异常基类
 *  @author liuwei
 *  @version 1.0
 *  @date 2017年5月14日
 */
public class NestedBusinessException extends NestedRuntimeException {

	private static final long serialVersionUID = 2724032065111817538L;

	String businessMes;

	public NestedBusinessException(String msg) {
		super(msg);
		businessMes = msg;
	}

	public NestedBusinessException(Throwable cause) {
		super(cause);
	}

	public NestedBusinessException(String msg, Throwable cause) {
		super(msg, cause);
		businessMes = msg;
	}

	/**
	 * Return the detail message, including the message from the business check
	 * exception if there is one.
	 */
	public String getMessage() {
		return super.getMessage(ExceptionDescriptor.Exception_BIZ);
	}

	public String getBusinessMessage() {
		return new StringBuffer("").append(businessMes).append("").toString();
	}
}
